package me.seu.demo.shardingsphere.repository;

import me.seu.demo.shardingsphere.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表 Dao
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:48
 */
@Mapper
public interface UserRepository extends BaseRepository<User, Long> {

}
