package me.seu.demo.shardingsphere.entity;

/**
 * 用户表
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:31
 */
public class User {

    Long userId;
    String userName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                '}';
    }

}
