package me.seu.demo.shardingsphere.repository;

import me.seu.demo.shardingsphere.entity.HealthLevel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 健康级别表 Dao
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:44
 */
@Mapper
public interface HealthLevelRepository extends BaseRepository<HealthLevel, Long> {

}
