package me.seu.demo.common.param;

/**
 * 搜索框参数.
 *
 * @author liangfeihu
 * @since 2023/7/12 15:08
 */
public interface TermType {
    String eq = "eq";

    String not = "not";
    String like = "like";
    String nlike = "nlike";
    String gt = "gt";
    String lt = "lt";
    String gte = "gte";
    String lte = "lte";
    String in = "in";
    String nin = "nin";
    String empty = "empty";
    String nempty = "nempty";
    String isnull = "isnull";
    String notnull = "notnull";
    String btw = "btw";
    String nbtw = "nbtw";
}
