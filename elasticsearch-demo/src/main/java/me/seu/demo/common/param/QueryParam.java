package me.seu.demo.common.param;

import com.alibaba.fastjson.JSONObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 查询参数.
 *
 * @author liangfeihu
 * @since 2023/7/12 14:55
 */
@Getter
@Setter
@Slf4j
public class QueryParam implements Serializable, Cloneable {

    private static final long serialVersionUID = 7941767360194797891L;
    public static final int DEFAULT_FIRST_PAGE_INDEX = 0;
    public static final int DEFAULT_PAGE_SIZE = 10;

    private boolean paging = true;
    private int firstPageIndex;
    private int pageIndex;
    private int pageSize;
    private List<Sort> sorts;
    private List<JSONObject> terms;

    /**
     * 构造方法.
     */
    public QueryParam() {
        this.firstPageIndex = DEFAULT_FIRST_PAGE_INDEX;
        this.pageIndex = this.firstPageIndex;
        this.pageSize = DEFAULT_PAGE_SIZE;
        this.sorts = new ArrayList<>();
        this.terms = new ArrayList<>();
    }


    /**
     * 从 JSON 获取条件.
     *
     * @param jsonList json集合对象
     */
    public static List<Term> getTermsFromJson(List<JSONObject> jsonList) {
        List<Term> terms = new ArrayList<>();
        for (JSONObject object : jsonList) {
            if (object.containsKey("terms")) {
                String termsStr = object.getJSONArray("terms").toJSONString();
                log.info("queryParam terms：{}", termsStr);
                List<Term> termList = JSONObject.parseArray(termsStr, Term.class);
                terms.addAll(termList);
            }
        }
        return terms;
    }

}
