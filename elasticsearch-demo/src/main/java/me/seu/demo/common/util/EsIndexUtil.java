package me.seu.demo.common.util;

import cn.hutool.core.date.DateUtil;
import java.util.Date;

/**
 * es 索引工具类
 *
 * @author liangfeihu
 * @since 2023/8/2 10:50
 */
public class EsIndexUtil {

    public static final String DEVICE_LOG_INDEX_PREFIX = "device_log_";
    public static final String DEVICE_EVENT_INDEX_PREFIX = "device_event_";
    public static final String DEVICE_PROPERTY_INDEX_PREFIX = "device_property_";


    public static String getDeviceLogIndex(String productId) {
        return DEVICE_LOG_INDEX_PREFIX + productId + "_" + getYearMonthStr();
    }

    public static String getDeviceEventIndex(String productId) {
        return DEVICE_EVENT_INDEX_PREFIX + productId + "_" + getYearMonthStr();
    }

    public static String getDevicePropertyIndex(String productId) {
        return DEVICE_PROPERTY_INDEX_PREFIX + productId + "_" + getYearMonthStr();
    }

    private static String getYearMonthStr() {
        return DateUtil.format(new Date(), "yyyy-M");
    }

}
