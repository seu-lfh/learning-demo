package me.seu.demo.common;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分页结果.
 *
 * @author liangfeihu
 * @since 2023/7/12 14:43
 */
@Data
@NoArgsConstructor
public class PagerResult<E> {

    private static final long serialVersionUID = -6171751136953308027L;

    private int pageIndex;

    private int pageSize;

    private long total;

    private List<E> data;

    /**
     * 构造方法.
     *
     * @param pageIndex 页码
     * @param pageSize  分页数
     * @param total     总数
     * @param data      数据
     */
    public PagerResult(int pageIndex, int pageSize, long total, List<E> data) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.total = total;
        this.data = data;
    }

}
