package me.seu.demo.common.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 排序参数.
 *
 * @author liangfeihu
 * @since 2023/7/12 14:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sort {

    /**
     * 排序方式.
     */
    private String order = "asc";

    /**
     * 属性名.
     */
    private String name;

    /**
     * 类型.
     */
    private String type;

}
