package me.seu.demo.common.model;

import cn.hutool.core.util.IdUtil;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * 设备消息日志存储对象
 *
 * @author liangfeihu
 * @since 2023/7/27 11:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Generated
public class DeviceLogEntity implements Serializable {

    private static final long serialVersionUID = -6849794470754667710L;

    private String id;

    private String deviceId;

    private String type;

    private long createTime;

    private String content;

    private String messageId;

    private long timestamp;

    /**
     * 设置Id
     */
    public DeviceLogEntity generateId() {
        if (StringUtils.isEmpty(id)) {
            setId(IdUtil.getSnowflake().nextIdStr());
        }
        return this;
    }

}
