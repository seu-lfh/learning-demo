package me.seu.demo.common.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 字段.
 *
 * @author liangfeihu
 * @since 2023/7/12 15:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Term {
    /**
     * 列.
     */
    private String column;

    /**
     * 值.
     */
    private String value;
    /**
     * 条件类别：and、or.
     */
    private String type;
    /**
     * termType.
     * @see TermType
     */
    private String termType;
}
