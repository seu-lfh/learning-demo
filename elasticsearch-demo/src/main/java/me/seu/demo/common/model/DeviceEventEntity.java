package me.seu.demo.common.model;

import cn.hutool.core.util.IdUtil;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * 设备事件
 *
 * @author liangfeihu
 * @since 2023/8/3 13:54
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceEventEntity implements Serializable {

    private static final long serialVersionUID = -1L;

    private String id;

    private String deviceId;

    private String eventSymbol;

    private String content;

    private String messageId;

    private long createTime;

    private long timestamp;

    /**
     * 设置Id
     */
    public DeviceEventEntity generateId() {
        if (StringUtils.isEmpty(id)) {
            setId(IdUtil.getSnowflake().nextIdStr());
        }
        return this;
    }

}
