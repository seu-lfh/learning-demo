package me.seu.demo.common.model;

import cn.hutool.core.util.IdUtil;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * 设备属性
 *
 * @author liangfeihu
 * @since 2023/7/27 13:37
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DevicePropertyEntity implements Serializable {

    private static final long serialVersionUID = -1L;

    private String id;

    private String deviceId;

    private String property;

    private String type;

    private String numberValue;

    private String value;

    private long createTime;

    private long timestamp;

    /**
     * 设置Id
     */
    public DevicePropertyEntity generateId() {
        if (StringUtils.isEmpty(id)) {
            setId(IdUtil.getSnowflake().nextIdStr());
        }
        return this;
    }

}
