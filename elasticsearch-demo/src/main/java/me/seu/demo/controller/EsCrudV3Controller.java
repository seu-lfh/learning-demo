package me.seu.demo.controller;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.common.model.DeviceLogEntity;
import me.seu.demo.common.model.DevicePropertyEntity;
import me.seu.demo.common.util.EsIndexUtil;
import me.seu.demo.handler.ElasticsearchDeviceLogHandler;
import me.seu.demo.handler.ElasticsearchDevicePropertyHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 往ES中增删改查数据
 *
 * @author liangfeihu
 * @since 2023/11/22 16:03
 */
@Slf4j
@RestController
@RequestMapping("/es/v3")
public class EsCrudV3Controller {

    @Autowired
    ElasticsearchDeviceLogHandler elasticsearchDeviceLogHandler;
    @Autowired
    ElasticsearchDevicePropertyHandler elasticsearchDevicePropertyHandler;

    @GetMapping("/insert")
    public ResponseEntity insertEsData() throws Exception {
        // 2.构造 设备上下线消息日志 存储
        DeviceLogEntity logEntity = new DeviceLogEntity(IdUtil.getSnowflake().nextIdStr(),
                "1669588983224303616", "online", System.currentTimeMillis(),
                "{\"headers\":{\"bindings\":[{\"id\":\"1579645201019809792\",\"type\":\"org\"},{\"id\":\"1593057879308148736\",\"type\":\"org\"},{\"id\":\"1638078076765933568\",\"type\":\"org\"}],\"serverId\":\"jetlinks-platform:8844\",\"deviceName\":\"一二三物联SD123-T10温湿度变送仪（modbus rtu）\",\"productName\":\"边缘网关186子设备产品\",\"connectTime\":1700210454812,\"from\":\"session\",\"productId\":\"1683757955242876929\",\"_uid\":\"Povl3SMAhmipGxdVAwEJrTMGH-41H60H\",\"creatorId\":\"1199596756811550720\",\"parentId\":\"1683756222311653376\"},\"messageType\":\"OFFLINE\",\"deviceId\":\"1717013019187015680\",\"timestamp\":1700368556800}",
                IdUtil.fastSimpleUUID(), System.currentTimeMillis());

       /* DeviceLogEntity logEntity2 = new DeviceLogEntity(IdUtil.getSnowflake().nextIdStr(),
                "1669588983224303616", "offline", System.currentTimeMillis(),
                "{\"headers\":{\"bindings\":[{\"id\":\"1579645201019809792\",\"type\":\"org\"},{\"id\":\"1593057879308148736\",\"type\":\"org\"},{\"id\":\"1638078076765933568\",\"type\":\"org\"}],\"serverId\":\"jetlinks-platform:8844\",\"deviceName\":\"一二三物联SD123-T10温湿度变送仪（modbus rtu）\",\"productName\":\"边缘网关186子设备产品\",\"connectTime\":1700210454812,\"from\":\"session\",\"productId\":\"1683757955242876929\",\"_uid\":\"Povl3SMAhmipGxdVAwEJrTMGH-41H60H\",\"creatorId\":\"1199596756811550720\",\"parentId\":\"1683756222311653376\"},\"messageType\":\"OFFLINE\",\"deviceId\":\"1717013019187015680\",\"timestamp\":1700368556800}",
                IdUtil.fastSimpleUUID(), System.currentTimeMillis());*/

        String deviceLogIndex = EsIndexUtil.getDeviceLogIndex("1669587233268400128");
        elasticsearchDeviceLogHandler.saveDeviceLog(deviceLogIndex, logEntity);
        // elasticsearchDeviceLogHandler.saveDeviceLog(deviceLogIndex, logEntity2);
        return ResponseEntity.ok("insert elasticsearch item data success");
    }

    @GetMapping("/insert/batch")
    public ResponseEntity insertEsDataList() throws Exception {
        String deviceLogIndex = EsIndexUtil.getDeviceLogIndex("1669587233268400128");
        List<DeviceLogEntity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DeviceLogEntity logEntity = new DeviceLogEntity(IdUtil.getSnowflake().nextIdStr(),
                    "1669588983224303616", getEventType(i), System.currentTimeMillis(),
                    "{\"headers\":{\"bindings\":[{\"id\":\"1579645201019809792\",\"type\":\"org\"},{\"id\":\"1593057879308148736\",\"type\":\"org\"},{\"id\":\"1638078076765933568\",\"type\":\"org\"}],\"serverId\":\"jetlinks-platform:8844\",\"deviceName\":\"一二三物联SD123-T10温湿度变送仪（modbus rtu）\",\"productName\":\"边缘网关186子设备产品\",\"connectTime\":1700210454812,\"from\":\"session\",\"productId\":\"1683757955242876929\",\"_uid\":\"Povl3SMAhmipGxdVAwEJrTMGH-41H60H\",\"creatorId\":\"1199596756811550720\",\"parentId\":\"1683756222311653376\"},\"messageType\":\"OFFLINE\",\"deviceId\":\"1717013019187015680\",\"timestamp\":1700368556800}",
                    IdUtil.fastSimpleUUID(), System.currentTimeMillis());
            list.add(logEntity);
        }
        elasticsearchDeviceLogHandler.saveDeviceLogList(deviceLogIndex, list);

        DeviceLogEntity logEntity2 = new DeviceLogEntity(IdUtil.getSnowflake().nextIdStr(),
                "1669588983224303616", "offline", System.currentTimeMillis(),
                "{\"headers\":{\"bindings\":[{\"id\":\"1579645201019809792\",\"type\":\"org\"},{\"id\":\"1593057879308148736\",\"type\":\"org\"},{\"id\":\"1638078076765933568\",\"type\":\"org\"}],\"serverId\":\"jetlinks-platform:8844\",\"deviceName\":\"一二三物联SD123-T10温湿度变送仪（modbus rtu）\",\"productName\":\"边缘网关186子设备产品\",\"connectTime\":1700210454812,\"from\":\"session\",\"productId\":\"1683757955242876929\",\"_uid\":\"Povl3SMAhmipGxdVAwEJrTMGH-41H60H\",\"creatorId\":\"1199596756811550720\",\"parentId\":\"1683756222311653376\"},\"messageType\":\"OFFLINE\",\"deviceId\":\"1717013019187015680\",\"timestamp\":1700368556800}",
                IdUtil.fastSimpleUUID(), System.currentTimeMillis());

        elasticsearchDeviceLogHandler.saveDeviceLog(deviceLogIndex, logEntity2);
        return ResponseEntity.ok("insert elasticsearch item batch data success");
    }

    private String getEventType(int i) {
        if (i % 3 == 0) {
            return "readPropertyReply";
        } else if (i % 3 == 1) {
            return "event";
        } else if (i % 3 == 2) {
            return "functionReply";
        } else {
            return "other";
        }
    }

    @GetMapping("/insert/property/batch")
    public ResponseEntity insertPropertyEsDataList() throws Exception {
        String devicePropertyIndex = EsIndexUtil.getDevicePropertyIndex("1669587233268400128");
        List<DevicePropertyEntity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            double value = RandomUtil.randomDouble(36, 2, RoundingMode.HALF_UP);
            DevicePropertyEntity pEntity = new DevicePropertyEntity(
                    IdUtil.getSnowflake().nextIdStr(), "1669588983224303616",
                    "temp_" + getPropertyKey(i), "int", value + "", value + "",
                    System.currentTimeMillis(), System.currentTimeMillis());
            list.add(pEntity);
        }
        elasticsearchDevicePropertyHandler.saveDevicePropertyList(devicePropertyIndex, list);
        return ResponseEntity.ok("insert elasticsearch property item batch data success");
    }

    private int getPropertyKey(int i) {
        return i % 4;
    }

}
