package me.seu.demo.handler;

import cn.hutool.core.util.ObjectUtil;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.mapping.DateProperty;
import co.elastic.clients.elasticsearch._types.mapping.KeywordProperty;
import co.elastic.clients.elasticsearch._types.mapping.Property;
import co.elastic.clients.elasticsearch._types.mapping.TextProperty;
import co.elastic.clients.elasticsearch._types.mapping.TypeMapping;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.indices.CreateIndexRequest;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import co.elastic.clients.elasticsearch.indices.ExistsRequest;
import co.elastic.clients.elasticsearch.indices.IndexSettings;
import co.elastic.clients.transport.endpoints.BooleanResponse;
import com.alibaba.fastjson.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.common.PagerResult;
import me.seu.demo.common.model.DeviceLogEntity;
import me.seu.demo.common.param.QueryParam;
import org.springframework.stereotype.Service;

/**
 * es store 设备日志消息
 * FIXME: 方法待修改完善
 *
 * @author liangfeihu
 * @since 2023/6/30 14:42
 */
@Slf4j
@Service
public class ElasticsearchDeviceLogHandler {

    @Resource(name = "coElasticsearchClient")
    private ElasticsearchClient elasticsearchClient;
    @Resource
    private ElasticsearchCommonHandler elasticsearchCommonHandler;

    /**
     * 新增单条
     */
    public void saveDeviceLog(String index, DeviceLogEntity deviceLog) throws Exception {
        if (!validateDeviceLogIndexExist(index)) {
            // 创建索引
            createDeviceLogIndex(index);
        }

        deviceLog.generateId();
        IndexRequest<DeviceLogEntity> indexRequest = new IndexRequest.Builder<DeviceLogEntity>()
                .index(index)
                .id(deviceLog.getId())
                .document(deviceLog)
                .build();
        IndexResponse response = elasticsearchClient.index(indexRequest);
        log.info("[saveDeviceLog] result={}", JSONObject.toJSONString(response));
    }

    /**
     * 批量新增
     */
    public void saveDeviceLogList(String index, List<DeviceLogEntity> logList) throws Exception {
        if (!validateDeviceLogIndexExist(index)) {
            // 创建索引
            createDeviceLogIndex(index);
        }
        BulkRequest.Builder bulkRequestBuilder = new BulkRequest.Builder();
        logList.stream()
                .forEach(item -> {
                            item.generateId();
                            bulkRequestBuilder.operations(operation -> operation
                                    .index(builder -> builder
                                            .index(index)
                                            .id(item.getId())
                                            .document(item)
                                    )
                            );
                        }
                );

        BulkResponse response = elasticsearchClient.bulk(bulkRequestBuilder.build());
        if (response.errors()) {
            for (BulkResponseItem item : response.items()) {
                if (ObjectUtil.isNotNull(item.error())) {
                    log.error("[saveLogList] to elasticsearch error: {}", item.error().reason());
                }
            }
        }
        log.info("[saveLogList] result={}", JSONObject.toJSONString(response));
    }

    /**
     * 分页查询
     */
    public PagerResult<DeviceLogEntity> queryPage(String index, String deviceId,
            QueryParam queryParam)
            throws Exception {
        if (!validateDeviceLogIndexExist(index)) {
            // 无索引 无数据
            new PagerResult<>(queryParam.getPageIndex(), queryParam.getPageSize(), 0,
                    new ArrayList<>());
        }
        // 根据条件构造参数查询
        SearchRequest searchRequest = elasticsearchCommonHandler.buildSearchRequest(index, deviceId,
                queryParam);

        // 返回对象List
        List<DeviceLogEntity> resList = new ArrayList<>();
        SearchResponse<DeviceLogEntity> searchResponse = elasticsearchClient.search(searchRequest,
                DeviceLogEntity.class);
        //命中list
        List<Hit<DeviceLogEntity>> hitList = searchResponse.hits().hits();
        long total = searchResponse.hits().total().value();
        for (Hit<DeviceLogEntity> hit : hitList) {
            DeviceLogEntity logEntity = hit.source();
            resList.add(logEntity);
        }

        log.info("[resList] listSize = {}", resList.size());
        return new PagerResult<>(queryParam.getPageIndex(), queryParam.getPageSize(), total,
                resList);
    }

    private boolean validateDeviceLogIndexExist(String indexName) throws Exception {
        BooleanResponse response = elasticsearchClient.indices()
                .exists(new ExistsRequest.Builder().index(indexName).build());
        return response.value();
    }

    private void createDeviceLogIndex(String indexName) throws Exception {
        //定义索引的别名
        String indexAliasName = indexName + "_alias";
        Map<String, Property> propertyMap = new HashMap<>();
        //构建索引类型
        propertyMap.put("id", new Property(new KeywordProperty.Builder().index(true).build()));
        propertyMap.put("deviceId",
                new Property(new KeywordProperty.Builder().index(true).build()));
        propertyMap.put("type", new Property(new KeywordProperty.Builder().index(true).build()));
        propertyMap.put("createTime", new Property(new DateProperty.Builder().index(true).build()));
        propertyMap.put("content",
                new Property(new TextProperty.Builder().index(true).analyzer("standard").build()));
        propertyMap.put("messageId",
                new Property(new KeywordProperty.Builder().index(true).build()));
        propertyMap.put("timestamp", new Property(new DateProperty.Builder().index(true).build()));

        //构建设置
        TypeMapping mapping = new TypeMapping.Builder().properties(propertyMap).build();
        IndexSettings indexSettings = new IndexSettings.Builder()
                .numberOfShards(String.valueOf(1))
                .numberOfReplicas(String.valueOf(1))
                .build();
        CreateIndexRequest createIndexRequest = new CreateIndexRequest.Builder()
                .index(indexName)
                .mappings(mapping)
                .settings(indexSettings)
                .build();
        //执行创建索引操作并返回结果
        CreateIndexResponse createIndexResponse = elasticsearchClient.indices()
                .create(createIndexRequest);
        log.info("[createDeviceLogIndex] indexName={} response={}", indexName,
                createIndexResponse.toString());
    }

}