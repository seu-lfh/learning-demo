package me.seu.demo.network;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * SpringBoot 启动类
 *
 * @author liangfeihu
 * @since 2023/06/18 13:35
 */
@SpringBootApplication
public class NetworkDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetworkDemoApplication.class, args);
    }

}
