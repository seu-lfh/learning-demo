package me.seu.demo;

import org.junit.Test;

/**
 * App Tests
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/10 上午9:38
 */
public class AppTests {

    @Test
    public void testString() {
        String s1 = new String("1") + new String("1");
        s1.intern();
        String s2 = "11";

        // false
        System.out.println(s1 == s2);

        String a = new String("abc").intern();
        String b = new String("abc").intern();
        if (a == b) {
            System.out.println("a==b");
        }
    }

}
