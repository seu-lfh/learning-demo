package me.seu.demo.service.netty.connection;

import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * tcp 客户端连接信息
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/8 下午5:25
 */
public class Connection {

    /**
     * 链接id,链接的唯一标示
     */
    private final long id;
    /**
     * 链接的ChannelHandlerContext
     */
    private ChannelHandlerContext ctx;

    /**
     * 设备唯一标识: IMEI
     */
    private volatile String deviceId;

    private boolean isSetDeviceId = false;

    private Lock lock = new ReentrantLock();

    /**
     * 附带属性
     */
    private ConcurrentHashMap<String, Object> attr = new ConcurrentHashMap<String, Object>();

    public Object getAttr(String key) {
        return attr.get(key);
    }

    public Object putAttr(String key, Object value) {
        return attr.put(key, value);
    }

    public Object removeAttr(String key) {
        return attr.remove(key);
    }

    public Connection(long id, ChannelHandlerContext ctx) {
        this.id = id;
        this.ctx = ctx;
    }

    public long getId() {
        return id;
    }

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    /**
     * 线程安全,没有验验证通过前，返回null
     */
    public String getDeviceId() {
        try {
            lock.lock();
            return deviceId;
        } finally {
            lock.unlock();
        }
    }

    /**
     * 设置deviceId，只可以设置一次，线程安全
     */
    public boolean isSetDeviceId(String deviceId) {
        try {
            lock.lock();
            if (isSetDeviceId) {
                return false;
            }
            this.deviceId = deviceId;
            isSetDeviceId = true;
            return true;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Connection other = (Connection) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + Integer.toHexString(super.hashCode()) + "->Connection [id=" + id
                + ", ctx=" + ctx + "]";
    }

}
