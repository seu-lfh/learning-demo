package me.seu.demo.service.wechat.message;

/**
 * robot msg interface
 *
 * @author liangfeihu
 * @since 2021/10/19 16:35
 */
public interface Message {

    /**
     * 返回消息的Json格式字符串
     *
     * @return 消息的Json格式字符串
     */
    String toJsonString();
}
