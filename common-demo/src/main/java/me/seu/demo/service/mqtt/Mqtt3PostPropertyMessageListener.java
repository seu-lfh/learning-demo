package me.seu.demo.service.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * MQTT 监听器
 *
 * @author liangfeihu
 * @since 2021/11/4 10:38
 */
public class Mqtt3PostPropertyMessageListener implements IMqttMessageListener {
    @Override
    public void messageArrived(String topic, MqttMessage payload) throws Exception {
        System.out.println("reply topic  : " + topic);
        System.out.println("reply payload: " + payload.toString());
    }
}
