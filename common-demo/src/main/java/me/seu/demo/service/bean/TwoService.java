package me.seu.demo.service.bean;

import org.springframework.stereotype.Component;

/**
 * Service Two
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/27 下午5:06
 */
@Component
public class TwoService extends BaseResource {
    private static final String API = "Two";

    public TwoService() {
        super(API);
    }

}
