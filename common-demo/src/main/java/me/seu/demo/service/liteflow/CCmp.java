package me.seu.demo.service.liteflow;

import com.yomahub.liteflow.core.NodeComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * liteFlow 组件3
 *
 * @author liangfeihu
 * @since 2022/11/8 10:24
 */
@Slf4j
@Component("c")
public class CCmp extends NodeComponent {
    @Override
    public void process() throws Exception {
        log.info("liteFlow 组件3: 处理中~~~");
    }
}
