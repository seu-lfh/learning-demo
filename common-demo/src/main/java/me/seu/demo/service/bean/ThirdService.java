package me.seu.demo.service.bean;

import org.springframework.stereotype.Component;

/**
 * Service Third
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/29 下午2:32
 */
@Component
public class ThirdService extends OneService {
    private static final String API = "Third";

    public ThirdService() {
        super(API);
    }

}
