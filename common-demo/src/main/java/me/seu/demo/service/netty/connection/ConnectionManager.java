package me.seu.demo.service.netty.connection;

import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 连接管理类，spring中单例
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/8 下午5:34
 */
@Slf4j
public class ConnectionManager {

    private static final ConnectionManager INSTANCE = new ConnectionManager();

    public static ConnectionManager getInstance() {
        return INSTANCE;
    }

    private ConnectionManager() {
    }

    private AtomicLong atomicLong = new AtomicLong();

    /**
     * key->deviceId, value->Connection
     */
    private ConcurrentHashMap<String, Connection> connMap = new ConcurrentHashMap<String, Connection>();

    public Connection getNewConnection(ChannelHandlerContext ctx) {
        Connection conn = new Connection(atomicLong.incrementAndGet(), ctx);
        return conn;
    }

    public Enumeration<String> keys() {
        return connMap.keys();
    }

    public int tcpSize() {
        return connMap.size();
    }

    public List<String> listAllConn() {
        Enumeration<String> keys = connMap.keys();
        List<String> connLst = new ArrayList<String>();
        while (keys.hasMoreElements()) {
            Connection c = connMap.get(keys.nextElement());
            if (c != null) {
                connLst.add(c.getDeviceId());
            }
        }
        return connLst;
    }

    public Connection addToConnMap(String key, Connection conn) {
        Connection oldConn = connMap.put(key, conn);
        if (oldConn == null) {
            // 新的key
            return null;
        }
        ChannelHandlerContext ctx = oldConn.getCtx();
        if (ctx == null) {
            // oldConn无效
            return oldConn;
        }
        if (oldConn.equals(conn)) {
            // 同key同value
            return null;
        }
        // 被替换的conn必须关闭
        ctx.pipeline().close();
        return oldConn;
    }

    public Connection getConn(String key) {
        return connMap.get(key);
    }

    /**
     * 设备下线，剔除连接
     */
    public Connection removeConn(String key) {
        return connMap.remove(key);
    }

    /**
     * 设备下线，剔除连接
     */
    public Connection removeConn(Connection conn) {
        Enumeration<String> keys = connMap.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            Connection v = connMap.get(key);
            if (conn.equals(v)) {
                return connMap.remove(key);
            }
        }
        return null;
    }

    /**
     * 优雅停机，关闭Tcp连接
     */
    public void closeAllConn() {
        Enumeration<String> keys = connMap.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            Connection v = connMap.get(key);
            log.info("[closeAllConn]close {} tcp connection~", key);
            v.getCtx().close();
        }
    }

}
