package me.seu.demo.service.json;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * <pre>
 * 物联网平台通过定义一种物的描述语言来描述物模型，
 * 称之为 TSL（即 Thing Specification Language)
 * </pre>
 *
 * @author liangfeihu
 * @since 2021/5/25 上午11:22
 */
@Data
@NoArgsConstructor
public class Tsl {
    private Profile profile;
    private List<Property> properties;
    private List<Event> events;
    private List<Service> services;

    public Tsl(Profile profile) {
        this.profile = profile;
    }

    @Data
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Profile {
        private String version;
        private String productKey;
    }

    @Data
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Property {
        private String identifier;
        private String propertyName;
        private String rwType;
        private String dataType;
        private Object dataSpecs;
        private Boolean requiredFlag;
        private String propertyDesc;
        public Property(String identifier, String propertyName, String rwType, Boolean requiredFlag, String propertyDesc) {
            this.identifier = identifier;
            this.propertyName = propertyName;
            this.rwType = rwType;
            this.requiredFlag = requiredFlag;
            this.propertyDesc = propertyDesc;
        }
    }

    @Data
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Event {
        private String identifier;
        private String eventName;
        private String method;
        private Integer eventType;
        private JSONArray outputdata;
        private Boolean requiredFlag;
        private String eventDesc;
    }

    @Data
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Service {
        private String identifier;
        private String serviceName;
        private String method;
        private Integer callType;
        private JSONArray inputParams;
        private JSONArray outputParams;
        private Boolean requiredFlag;
        private String serviceDesc;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }

}
