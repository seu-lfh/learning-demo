package me.seu.demo.service.netty.message;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 基础数据包（上行）
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/1 上午10:30
 */
@Data
@NoArgsConstructor
public class BasicMessage {

    private byte protocolNo;

    private byte[] packetContent;

    private byte[] packetSeqNum;

    public BasicMessage(byte protocolNo, byte[] packetContent, byte[] packetSeqNum) {
        this.protocolNo = protocolNo;
        this.packetContent = packetContent;
        this.packetSeqNum = packetSeqNum;
    }

}
