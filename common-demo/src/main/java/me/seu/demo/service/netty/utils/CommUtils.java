package me.seu.demo.service.netty.utils;

import io.netty.channel.ChannelHandlerContext;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.netty.constants.SysConstants;

import java.util.Arrays;

/**
 * CommUtils
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/1 上午11:12
 */
@Slf4j
public class CommUtils {

    /**
     * 获取年月日 时分秒
     */
    public static String handleDateTime(byte[] packetContent) {
        int year = (0xFF & packetContent[0]) + 2000;
        int month = 0xFF & packetContent[1];
        int day = 0xFF & packetContent[2];
        int hour = 0xFF & packetContent[3];
        int minute = 0xFF & packetContent[4];
        int second = 0xFF & packetContent[5];
        StringBuilder builder = new StringBuilder();
        builder.append(year)
                .append("-").append(month < 10 ? "0" + month : month)
                .append("-").append(day < 10 ? "0" + day : day)
                .append(" ");
        builder.append(hour < 10 ? "0" + hour : hour).append(":")
                .append(minute < 10 ? "0" + minute : minute).append(":")
                .append(second < 10 ? "0" + second : second);
        return builder.toString();
    }

    /**
     * 获取deviceId
     */
    public static String getDeviceIdFromByte(byte[] content) throws Exception {
        if (content == null || content.length <= 8) {
            throw new Exception("获取deviceId,数据长度错误");
        }
        // 例：IMEI 号为 123456789123456，则终端 ID 为：0x01 0x23 0x45 0x67 0x89 0x12 0x34 0x56
        byte[] bytesNew = Arrays.copyOf(content, 8);
        String deviceId = ByteUtils.bytesToHexString(bytesNew);
        if ('0' == deviceId.charAt(0)) {
            return deviceId.substring(1);
        }
        return deviceId;
    }

    /**
     * 将deviceId设置到channel的attr中
     */
    public static void setDeviceIdAttr(ChannelHandlerContext ctx, String deviceId) {
        Attribute<String> macAttr = ctx.channel().attr(SysConstants.DEVICE_ID);
        macAttr.set(deviceId);
    }

    /**
     * 从channel的attr中获取deviceId
     */
    public static String getDeviceIdFromAttr(ChannelHandlerContext ctx) {
        Attribute<String> macAttr = ctx.channel().attr(SysConstants.DEVICE_ID);
        return macAttr.get();
    }

    public static String getAlertDesc(byte alert) {
        if (0x00 == alert) {
            return "正常";
        } else if (0x01 == alert) {
            return "SOS 求救";
        } else if (0x02 == alert) {
            return "断电报警";
        } else if (0x03 == alert) {
            return "震动报警";
        } else if (0x04 == alert) {
            return "进围栏报警";
        } else if (0x05 == alert) {
            return "出围栏报警";
        } else if (0x06 == alert) {
            return "超速报警";
        } else if (0x09 == alert) {
            return "位移报警";
        } else if (0x0A == alert) {
            return "伪基站报警";
        } else if (0x0B == alert) {
            return "检测到伪基站(实时检测,不作为报警)";
        } else if (0x0C == alert) {
            return "非法拆除报警";
        }
        return "未知";
    }

    public static String getVoltageLevelDesc(byte voltageLevel) {
        if (0x00 == voltageLevel) {
            return "无电（关机）";
        } else if (0x01 == voltageLevel) {
            return "电量极低（不足以打电话发短信等）";
        } else if (0x02 == voltageLevel) {
            return "电量很低（低电报警）";
        } else if (0x03 == voltageLevel) {
            return "电量低（可正常使用）";
        } else if (0x04 == voltageLevel) {
            return "电量中";
        } else if (0x05 == voltageLevel) {
            return "电量高";
        } else if (0x06 == voltageLevel) {
            return "电量极高";
        }
        return "未知";
    }

    public static String getGsmSignalLevelDesc(byte gsmLevel) {
        if (0x00 == gsmLevel) {
            return "无信号";
        } else if (0x01 == gsmLevel) {
            return "信号极弱";
        } else if (0x02 == gsmLevel) {
            return "信号较弱";
        } else if (0x03 == gsmLevel) {
            return "信号良好";
        } else if (0x04 == gsmLevel) {
            return "信号强";
        }
        return "未知";
    }

    public static String getLanguageDesc(byte language) {
        if (0x00 == language) {
            return "不需要平台回复";
        } else if (0x01 == language) {
            return "中文";
        } else if (0x02 == language) {
            return "英文";
        }
        return "未知";
    }

}
