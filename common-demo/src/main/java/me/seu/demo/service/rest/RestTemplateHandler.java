package me.seu.demo.service.rest;

import com.alibaba.fastjson.JSONObject;
import me.seu.demo.common.model.APIResult;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author liangfeihu
 * @since 2020/6/17 17:49
 */
public class RestTemplateHandler {

    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();

        String tokenUrl = "http://127.0.0.1:8080/device/sync/9202";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject params = new JSONObject();
        params.put("clientId", "15214353025");
        params.put("channelNo", 1);
        params.put("playbackMode", 2);
        params.put("playbackSpeed", 0);
        params.put("messageId", 0x9202);

        HttpEntity<JSONObject> requestEntity = new HttpEntity<>(params, headers);
        // 执行HTTP请求
        ResponseEntity<APIResult> response = restTemplate.postForEntity(tokenUrl, requestEntity, APIResult.class);
        // 输出结果
        APIResult body = response.getBody();
        System.out.println(response.getStatusCodeValue() + " body=" + JSONObject.toJSONString(body));
    }

}
