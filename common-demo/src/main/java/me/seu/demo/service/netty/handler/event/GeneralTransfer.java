package me.seu.demo.service.netty.handler.event;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.netty.handler.Process;
import me.seu.demo.service.netty.message.GpsMessage;
import me.seu.demo.service.netty.utils.ByteUtils;

import java.util.Arrays;

/**
 * 信息传输通用包 0x94
 * 服务器无需回复该类型包
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/6 下午5:23
 */
@Slf4j
public class GeneralTransfer implements Process {
    private final byte OUTER_VOLTAGE = 0x00;
    private final byte STATE_SYNC = 0x04;
    private final byte STATE_IO = 0x05;
    private final byte CHECK_ARGS = 0x06;

    @Override
    public void execute(GpsMessage msg) throws Exception {
        byte[] packetContent = msg.getPacketContent();
        int length = packetContent.length;
        if (length < 1) {
            return;
        }
        byte infoType = packetContent[0];
        byte[] content = Arrays.copyOfRange(packetContent, 1, length);
        String hexType = ByteUtils.byteToHexString(infoType);
        log.info("[GeneralTransfer]infoType=0x{} data=0x{}", hexType, ByteUtils.bytesToHexString(content));
        if (OUTER_VOLTAGE == infoType) {
            byte[] intArr = new byte[4];
            intArr[0] = 0x00;
            intArr[1] = 0x00;
            intArr[2] = content[0];
            intArr[3] = content[1];
            int anInt = ByteUtils.byteArrayToInt(intArr);
            double voltage = anInt / 100d;
            log.info("[GeneralTransfer]infoType=0x{} voltage={}", hexType, voltage);
        } else if (STATE_SYNC == infoType || CHECK_ARGS == infoType) {
            String dataStr = new String(content);
            log.info("[GeneralTransfer]infoType=0x{} dataStr={}", hexType, dataStr);
        }

    }
}
