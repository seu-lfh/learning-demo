package me.seu.demo.service.netty.handler;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.netty.enums.EventEnum;
import me.seu.demo.service.netty.handler.event.AlertGps;
import me.seu.demo.service.netty.handler.event.GpsLocation;
import me.seu.demo.service.netty.handler.event.HandShake;
import me.seu.demo.service.netty.handler.event.HeartBeat;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * 消息处理器管理类
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/3/29 上午9:50
 */
@Slf4j
@Component
public class ProcessManager {

    private static ProcessManager INSTANCE;

    public Map<EventEnum, Process> processMap = new HashMap<EventEnum, Process>();

    public static ProcessManager getInstance() {
        return INSTANCE;
    }

    public Process getProcess(EventEnum eventEnum) {
        return processMap.get(eventEnum);
    }

    /**
     * 初始化处理过程 在此处添加处理接口
     */
    @PostConstruct
    private void init() {
        log.info("init the msg handler INSTANCE");
        INSTANCE = new ProcessManager();
        // 登录验证
        INSTANCE.processMap.put(EventEnum.LOGIN, new HandShake());
        // 心跳包
        INSTANCE.processMap.put(EventEnum.HEARTBEAT, new HeartBeat());
        // GPS 定位包
        GpsLocation gpsLocation = new GpsLocation();
        INSTANCE.processMap.put(EventEnum.GPS_LOCATION, gpsLocation);
        // GPS单围栏、多围栏 报警包处理
        AlertGps alertGps = new AlertGps();
        INSTANCE.processMap.put(EventEnum.ALERT_GPS, alertGps);

        // TODO 注册具体的 消息事件处理器

    }

}
