package me.seu.demo.service.json;

import java.util.ArrayList;

/**
 * 空 tsl test
 *
 * @author liangfeihu
 * @since 2021/6/23 17:27
 */
public class TslTest {

    public static void main(String[] args) {
        Tsl tsl = new Tsl();
        Tsl.Profile profile = new Tsl.Profile();
        profile.setProductKey("xxxxxxxx");
        tsl.setProfile(profile);
        tsl.setProperties(new ArrayList<>());
        tsl.setEvents(new ArrayList<>());
        tsl.setServices(new ArrayList<>());

        System.out.println(tsl.toString());
    }

}
