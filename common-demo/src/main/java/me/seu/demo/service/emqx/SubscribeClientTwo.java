package me.seu.demo.service.emqx;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;

/**
 * 订阅客户端
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/28 下午5:11
 */
public class SubscribeClientTwo {
//    public static final String HOST = "tcp://127.0.0.1:1883";
//    public static final String TOPIC = "device_config2";
//    private static final String clientId = "emqx_test_java_three";

    public static final String HOST = "tcp://mqtt.tongshuyun.com:1883";
    public static final String TOPIC = "/sys/lyrkb7Sc8AR1/MAM01202108000088/thing/event/op_report/post_reply";
    private static final String clientId = "lyrkb7Sc8AR1.MAM01202108000088|timestamp=1655260350725,_v=paho-java-1.0.0,securemode=2,signmethod=hmacsha256|";

    private MqttClient client;
    private MqttConnectOptions options;
    private String userName = "MAM01202108000088&lyrkb7Sc8AR1";
    private String passWord = "2629df2d35f909b2d9338148bc9dc99321fbc6b7d33653366d7ad13b981511f1";

    private String messageStr = "{\n" +
            "    \"id\":123,\n" +
            "    \"version\":null,\n" +
            "    \"method\":null,\n" +
            "    \"params\":{\n" +
            "        \"lm\":2,\n" +
            "        \"loc\":\"I/HA73XFynPSu27gow1gIQ==\",\n" +
            "        \"ver\":\"1.4.19\",\n" +
            "        \"signalFor4G\":31,\n" +
            "        \"vbatt\":3589,\n" +
            "        \"cDatas\":[\n" +
            "            {\n" +
            "                \"isWork\":0,\n" +
            "                \"temp\":28,\n" +
            "                \"cModel\":2,\n" +
            "                \"gyY\":-0.001,\n" +
            "                \"dStatus\":5,\n" +
            "                \"gyX\":-0.002,\n" +
            "                \"noise\":37,\n" +
            "                \"cInterval\":2,\n" +
            "                \"gyZ\":0.001,\n" +
            "                \"cTime\":\"1655192742000\",\n" +
            "                \"encAccRates\":\"wjFiHMUzE7lkvE73wXzpfXSJAPIP40XYyMD0H2623z4=\",\n" +
            "                \"opThr\":225\n" +
            "            }\n" +
            "        ],\n" +
            "        \"reqId\":\"\"\n" +
            "    }\n" +
            "}";

    private void start() {
        try {
            // host为主机名，test为clientid即连接MQTT的客户端ID，一般以客户端唯一标识符表示，
            // MemoryPersistence设置clientid的保存形式，默认为以内存保存
            client = new MqttClient(HOST, clientId, new MemoryPersistence());
            // MQTT的连接设置
            options = new MqttConnectOptions();
            // 设置是否清空session,这里如果设置为false表示服务器会保留客户端的连接记录，
            // 这里设置为true表示每次连接到服务器都以新的身份连接
            options.setCleanSession(true);
            // 设置连接的用户名
            options.setUserName(userName);
            // 设置连接的密码
            options.setPassword(passWord.toCharArray());
            // 设置超时时间 单位为秒
            options.setConnectionTimeout(10);
            // 设置会话心跳时间 单位为秒
            // 服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
            options.setKeepAliveInterval(20);
            // 设置回调
            client.setCallback(new OnMessageCallback2());

            MqttTopic topic = client.getTopic(TOPIC);
            // setWill方法，如果项目中需要知道客户端是否掉线可以调用该方法。
            // 设置最终端口的通知消息
            //options.setWill(topic, "close".getBytes(), 0, true);

            client.connect(options);
            // 订阅消息
            int[] Qos = {1};
            String[] topics = {TOPIC};
            client.subscribe(topics, Qos);

            Thread.sleep(2 * 1000);

            MqttMessage message = new MqttMessage(messageStr.getBytes(StandardCharsets.UTF_8));
            client.publish("/sys/lyrkb7Sc8AR1/MAM01202108000088/thing/event/op_report/post", message);

            client.publish("/sys/lyrkb7Sc8AR1/MAM01202108000088/thing/event/op_report/post", message);

            client.publish("/sys/lyrkb7Sc8AR1/MAM01202108000088/thing/event/op_report/post", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            client.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {

        for (int i = 0; i < 10; i++) {
            Thread.sleep(10 * 1000);

            SubscribeClientTwo client = new SubscribeClientTwo();
            client.start();

            Thread.sleep(10 * 1000);

            client.disconnect();
        }

    }

}
