package me.seu.demo.service.json;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * json tsl 解析工具
 *
 * @author liangfeihu
 * @since 2021/5/28 下午4:18
 */
public class TslJsonParser {

    public static void main(String[] args) throws IOException {
        String jsonFilePath = "/Users/a123/codes/myown/learning-demo/common-demo/src/main/resources/product_tsl.json";
        File file = new File(jsonFilePath );
        String inputJson = FileUtils.readFileToString(file,"UTF-8");

        System.out.println(inputJson);

        System.out.println("----------------");

        System.out.println(JSONObject.toJSON(null));

        //Tsl tsl = parseTsl(inputJson);

        //System.out.println(tsl.toString());

        String tslDemo = "{\n" +
                "\t\"profile\":{\n" +
                "\t\t\"productKey\":\"HmcslTyyzOdM\",\n" +
                "\t\t\"version\":\"1\"\n" +
                "\t},\n" +
                "\t\"services\":[],\n" +
                "\t\"events\":[],\n" +
                "\t\"properties\":[\n" +
                "\t\t{\n" +
                "\t\t\t\"identifier\":\"canshu\",\n" +
                "\t\t\t\"dataSpecs\":{\n" +
                "\t\t\t\t\"min\":1,\n" +
                "\t\t\t\t\"unitName\":\"无\",\n" +
                "\t\t\t\t\"max\":3,\n" +
                "\t\t\t\t\"step\":1\n" +
                "\t\t\t},\n" +
                "\t\t\t\"rwType\":\"rw\",\n" +
                "\t\t\t\"propertyName\":\"canshu\",\n" +
                "\t\t\t\"dataType\":\"int32\",\n" +
                "\t\t\t\"requiredFlag\":false\n" +
                "\t\t}\n" +
                "\t]\n" +
                "}";

        JSONObject object = JSONObject.parseObject(tslDemo);

        System.out.println(object.toJSONString());

        String ts = "{\"profile\":{\"productKey\":\"HmcslTyyzOdM\",\"version\":\"1\"},\"services\":[],\"events\":[],\"properties\":[{\"identifier\":\"canshu\",\"dataSpecs\":{\"min\":1,\"unitName\":\"无\",\"max\":3,\"step\":1},\"rwType\":\"rw\",\"propertyName\":\"canshu\",\"dataType\":\"int32\",\"requiredFlag\":false}]}\n";
    }

    public static Tsl parseTsl(String tslJson) {
        // 物模型json解析
        JSONObject jsonObject = com.alibaba.fastjson.JSON.parseObject(tslJson);
        Tsl.Profile profile = jsonObject.getObject("profile", Tsl.Profile.class);
        System.out.println(profile.getProductKey() + " " + profile.getVersion());
        List<Tsl.Property> properties = jsonObject.getObject("properties", List.class);
        Tsl.Property property = properties.get(0);
        System.out.println(property.getIdentifier() + ":" + property.getPropertyName());
        List<Tsl.Event> events = jsonObject.getObject("events", List.class);
        Tsl.Event event = events.get(0);
        System.out.println(event.getIdentifier() + ":" + event.getEventName() + ":" + event.getEventType());
        List<Tsl.Service> services = jsonObject.getObject("services", List.class);
        Tsl.Service service = services.get(0);
        System.out.println(service.getIdentifier() + ":" + service.getServiceName() + ":" + service.getServiceDesc());
        Tsl tsl = new Tsl();
        tsl.setProfile(profile);
        tsl.setProperties(properties);
        tsl.setEvents(events);
        tsl.setServices(services);
        return tsl;
    }

}
