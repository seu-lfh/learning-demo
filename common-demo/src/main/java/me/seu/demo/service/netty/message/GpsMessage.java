package me.seu.demo.service.netty.message;

import io.netty.channel.ChannelHandlerContext;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 通用GPS上行消息
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/1 上午10:57
 */
@Data
@NoArgsConstructor
public class GpsMessage extends BasicMessage {

    private String deviceId;
    private ChannelHandlerContext ctx;

    public GpsMessage(String deviceId) {
        this.deviceId = deviceId;
    }

    public GpsMessage(byte protocolNo, byte[] packetContent, byte[] packetSeqNum) {
        super(protocolNo, packetContent, packetSeqNum);
    }

    public GpsMessage(byte protocolNo, byte[] packetContent, byte[] packetSeqNum, String deviceId) {
        super(protocolNo, packetContent, packetSeqNum);
        this.deviceId = deviceId;
    }

}
