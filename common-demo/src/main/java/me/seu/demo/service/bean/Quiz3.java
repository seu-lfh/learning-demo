package me.seu.demo.service.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Bean 初始化执行顺序 @PostConstruct InitializingBean.afterPropertiesSet()。
 * Bean 销毁回调顺序 @PreDestroy DisposableBean.destroy() 。
 *
 * @author liangfeihu
 * @since 2021/10/7 19:55
 */
@Controller
public class Quiz3 implements InitializingBean, DisposableBean {
    @PostConstruct
    public void init() {
        System.out.println(1);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println(2);
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println(3);
    }

    @Override
    public void destroy() throws Exception {
        System.out.println(4);
    }

    @GetMapping("/hello")
    @ResponseBody
    public String hello() {
        System.out.println("hello aspect");
        return "system check hello success";
    }

}
