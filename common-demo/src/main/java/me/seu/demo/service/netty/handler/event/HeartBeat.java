package me.seu.demo.service.netty.handler.event;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.netty.handler.Process;
import me.seu.demo.service.netty.message.GpsMessage;
import me.seu.demo.service.netty.utils.CommUtils;
import me.seu.demo.service.netty.utils.NetUtils;

/**
 * 心跳包处理 0x13
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/1 下午2:18
 */
@Slf4j
public class HeartBeat implements Process {

    @Override
    public void execute(GpsMessage msg) throws Exception {
        log.info("[HeartBeat]packet deviceId:{}", msg.getDeviceId());
        handleHeartBeatPacket(msg.getDeviceId(), msg.getPacketContent());

        // 构造心跳响应包
        byte[] replyPacket = NetUtils.buildCommonReplyPacket(msg);
        ChannelHandlerContext ctx = msg.getCtx();
        if (ctx.channel().isWritable()) {
            // 将心跳响应包发送出去
            ctx.pipeline().writeAndFlush(Unpooled.wrappedBuffer(replyPacket));
        } else {
            ctx.pipeline().close();
        }
    }

    private void handleHeartBeatPacket(String deviceId, byte[] packetContent) {
        byte deviceInfo = packetContent[0];
        String deviceStr = getDeviceStatusInfo(deviceInfo);
        log.info("[HeartBeat]deviceId={} deviceInfo={} ", deviceId, deviceStr);
        byte voltageLevel = packetContent[1];
        String voltageLevelDesc = CommUtils.getVoltageLevelDesc(voltageLevel);
        // GSM信号等级
        byte gsmLevel = packetContent[2];
        String gsmSignalLevelDesc = CommUtils.getGsmSignalLevelDesc(gsmLevel);
        log.info("[HeartBeat]deviceId={} voltageLevel={} gsmSignalLevel={}",
                deviceId, voltageLevelDesc, gsmSignalLevelDesc);
        // 报警语言详解
        byte alert = packetContent[3];
        String alertDesc = CommUtils.getAlertDesc(alert);
        byte language = packetContent[4];
        String languageDesc = CommUtils.getLanguageDesc(language);
        log.info("[HeartBeat] alert={} language={}", alertDesc, languageDesc);
    }

    private String getDeviceStatusInfo(byte deviceInfo) {
        StringBuilder builder = new StringBuilder();
        int one = 0x00000001 & (deviceInfo >> 7);
        builder.append(one == 0 ? "油电接通" : "油电断开").append("、");
        int two = 0x00000001 & (deviceInfo >> 6);
        builder.append(two == 1 ? "GPS 已定位" : "GPS 未定位").append("、");

        byte alert = (byte) (0x00000007 & (deviceInfo >> 3));
        builder.append(getAlertDesc(alert)).append("、");

        int three = 0x00000001 & (deviceInfo >> 2);
        builder.append(three == 1 ? "已接电源充电" : "未接电源充电").append("、");
        int four = 0x00000001 & (deviceInfo >> 1);
        builder.append(four == 1 ? "ACC 高" : "ACC 低");
        return builder.toString();
    }

    public static String getAlertDesc(byte alert) {
        if (0x00 == alert) {
            return "正常";
        } else if (0x01 == alert) {
            return "震动报警";
        } else if (0x02 == alert) {
            return "断电报警";
        } else if (0x03 == alert) {
            return "低电报警";
        } else if (0x05 == alert) {
            return "超时报警）";
        } else if (0x06 == alert) {
            return "超速报警";
        }
        return "未知";
    }

}
