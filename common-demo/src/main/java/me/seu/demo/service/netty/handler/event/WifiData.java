package me.seu.demo.service.netty.handler.event;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.netty.handler.Process;
import me.seu.demo.service.netty.message.GpsMessage;
import me.seu.demo.service.netty.utils.ByteUtils;
import me.seu.demo.service.netty.utils.CommUtils;

import java.util.Arrays;

/**
 * 终端发送 WiFi 包 0x2C
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/7 下午2:10
 */
@Slf4j
public class WifiData implements Process {

    @Override
    public void execute(GpsMessage msg) throws Exception {
        byte[] packetContent = msg.getPacketContent();
        // 处理年月日
        String dateTime = CommUtils.handleDateTime(packetContent);
        log.info("[WifiData]dateTime={}", dateTime);
        // 处理MCC、MNC、LAC、Cell ID、RSSI
        handleMccAndCell(packetContent);
        // 处理NLAC NCI NRSSI
        handleNlacAndRssi(packetContent, 15);
        // 处理wifi mac数量
        byte[] bytes = Arrays.copyOfRange(packetContent, 51, packetContent.length);
        handleWifiMacInfo(bytes);

        // 该wifi包不需要回复
    }

    private void handleWifiMacInfo(byte[] bytes) {
        int timeBefore = 0xFF & bytes[0];
        int wifiNum = 0xFF & bytes[1];
        log.info("[WifiData] timeBefore={} wifiNum={}", timeBefore, wifiNum);
        if (wifiNum > 0) {
            for (int i = 0; i < wifiNum; i++) {
                int step = i * 7;
                byte[] mac = Arrays.copyOfRange(bytes, 2 + step, 2 + step + 6);
                byte wifiStrength = bytes[2 + step + 6];
                log.info("[WifiData] wifi_mac index={} mac=0x{} wifiStrength=0x{}", i, ByteUtils.bytesToHexString(mac), ByteUtils.byteToHexString(wifiStrength));
            }
        }
    }

    private void handleNlacAndRssi(byte[] packetContent, int index) {
        for (int i = 0; i < 6; i++) {
            int step = i * 6;
            byte[] lac = new byte[]{packetContent[index + step], packetContent[index + step + 1]};
            int lacNum = ByteUtils.byteArrayToInt(lac, true);
            byte[] cell = new byte[]{0x00, packetContent[index + step + 2], packetContent[index + step + 3], packetContent[index + step + 4]};
            int cellId = ByteUtils.byteArrayToInt(cell);
            byte rssi = packetContent[index + step + 5];
            log.info("[WifiData]index={} lacNum:{} cellId:{} rssi=0x{}", i, lacNum, cellId, ByteUtils.byteToHexString(rssi));
        }
    }

    private void handleMccAndCell(byte[] packetContent) {
        byte[] mcc = new byte[]{packetContent[6], packetContent[7]};
        int mccNum = ByteUtils.byteArrayToInt(mcc, true);
        int mncNum = 0xFF & packetContent[8];
        byte[] lac = new byte[]{packetContent[9], packetContent[10]};
        int lacNum = ByteUtils.byteArrayToInt(lac, true);
        byte[] cell = new byte[]{0x00, packetContent[11], packetContent[12], packetContent[13]};
        int cellId = ByteUtils.byteArrayToInt(cell);
        byte rssi = packetContent[14];
        log.info("[WifiData]mccNum:{} mncNum:{} lacNum:{} cellId:{} rssi=0x{}", mccNum, mncNum, lacNum, cellId, ByteUtils.byteToHexString(rssi));
    }

}
