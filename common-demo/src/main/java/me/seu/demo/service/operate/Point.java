package me.seu.demo.service.operate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 点
 *
 * @author liangfeihu
 * @since 2022/9/7 11:01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Point {

    private Integer start;

    private Integer end;

}
