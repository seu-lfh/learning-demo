package me.seu.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.Properties;

/**
 * MailSenderService
 *
 * @author liangfeihu
 * @since 2021/1/25 下午5:14
 */
@Slf4j
@Service
public class MailSenderService {

    @Resource
    private JavaMailSenderImpl mailSender;

    public void sendMail() {
        Properties mailProperties = mailSender.getJavaMailProperties();
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom(mailSender.getUsername());
        // 可以是一个数组
        msg.setTo(mailProperties.getProperty("receiver").split(","));
        msg.setText("nice day");
        msg.setSubject("just test");

        this.mailSender.send(msg);
    }

    public void sendMailAttach() throws Exception {
        Properties mailProperties = mailSender.getJavaMailProperties();
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom(mailSender.getUsername());
        helper.setTo(mailProperties.getProperty("receiver").split(","));
        // 第二个参数为是否支持html，如果为true，则表示支持，此时如果写入<h2>nice</h2>，则nice会被加粗，默认为false
        helper.setText("nice day2", true);
        helper.setSubject("just test2");
        // 或者使用new FiledataSource
        FileSystemResource file = new FileSystemResource(new File("/Users/liangfeihu/projects/gitee/myown/learning-demo/common-demo/src/main/resources/excel/daily_bill_excel_template.xlsx"));
        helper.addAttachment("bill_excel.xlsx", file);

        mailSender.send(message);
    }

    public void sendMailAttachPlus() throws Exception {
        MimeMessage message = mailSender.createMimeMessage();
        Properties mailProperties = mailSender.getJavaMailProperties();
        String day = DateFormatUtils.format(new Date(), "yyyyMMdd");

        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom(mailSender.getUsername());
        helper.setTo(mailProperties.getProperty("receiver").split(","));
        // 第二个参数为是否支持html，如果为true，则表示支持，此时如果写入<h2>nice</h2>，则nice会被加粗，默认为false
        helper.setText("各位好，附件为字节跳动" + day + "台账，请查收", true);
        helper.setSubject("字节跳动每日台账");

        File file = new File("/Users/liangfeihu/projects/gitee/myown/learning-demo/common-demo/src/main/resources/excel/daily_bill_excel_template.xlsx");
        byte[] bt = fileConvertToByteArray(file);
        //这行解决文件名称乱码
        String newFileName = MimeUtility.encodeWord("字节跳动台账" + day + ".xlsx", "utf-8", "B");
        helper.addAttachment(newFileName, new ByteArrayResource(bt));

        mailSender.send(message);
    }

    /**
     * 把一个文件转化为byte字节数组。
     *
     * @return
     */
    private byte[] fileConvertToByteArray(File file) {
        byte[] data = null;

        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            int len;
            byte[] buffer = new byte[1024];
            while ((len = fis.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }

            data = baos.toByteArray();

            fis.close();
            baos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }


}
