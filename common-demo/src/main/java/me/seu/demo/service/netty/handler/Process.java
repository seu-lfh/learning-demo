package me.seu.demo.service.netty.handler;

import me.seu.demo.service.netty.message.GpsMessage;

/**
 * 业务消息处理接口
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/3/29 上午9:49
 */
public interface Process {

    /**
     * 业务逻辑处理
     *
     * @param msg
     * @throws Exception
     */
    void execute(GpsMessage msg) throws Exception;

}
