package me.seu.demo.service.bean;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Service;

/**
 * 依次的增强类型为 Around.class, Before, After.class, AfterReturning.class, AfterThrowing.class。
 *
 * @author liangfeihu
 * @since 2021/10/7 20:03
 */
@Aspect
@Service
public class Quiz5 {
    @Before("execution(* me.seu.demo.service.bean.Quiz3.hello()) ")
    public void aspect1(JoinPoint pjp) {
        System.out.println("Before");
    }

    @Around("execution(* me.seu.demo.service.bean.Quiz3.hello()) ")
    public Object aspect2(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("Before Around");
        Object proceed = pjp.proceed();
        System.out.println("After Around " + proceed);
        return proceed;
    }

    @After("execution(* me.seu.demo.service.bean.Quiz3.hello()) ")
    public void aspect3(JoinPoint pjp) {
        System.out.println("After");
    }

    @AfterReturning("execution(* me.seu.demo.service.bean.Quiz3.hello()) ")
    public void aspect4(JoinPoint pjp) {
        System.out.println("AfterReturning");
    }
}
