package me.seu.demo.service.netty.constants;

/**
 * GPS设备协议号
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/1 上午9:44
 */
public class ProtocolConstants {

    public static final byte[] PACKET_START = new byte[]{0x78, 0x78};
    public static final byte[] PACKET_END = new byte[]{0x0D, 0x0A};

    /**
     * 登录包 0x01
     * 心跳包 0x13 状态信息包
     * GPS 定位包（UTC） 0x22 采用GPS、LBS 合并信息包
     * 报警数据（UTC）   0x26 掉电报警、超速报警
     * 服务器发送在线指令 0x80
     * 终端在线指令回复  0x15
     */
    public static final byte PROTOCOL_LOGIN = 0x01;
    public static final byte PROTOCOL_HEARTBEAT = 0x13;
    public static final byte PROTOCOL_GPS_LOCATION = 0x22;
    public static final byte PROTOCOL_ALERT_GPS = 0x26;
    public static final byte PROTOCOL_ONLINE_INSTRUCTION = (byte) 0x80;
    public static final byte PROTOCOL_ONLINE_INSTRUCTION_REPLY = 0x15;

    // under below protocol no use

    /**
     * 校时包 0x8A
     * 终端发送 WiFi 包 0x2C
     * GPS 地址请求包（UTC） 0x2A
     * LBS 多基站扩展信息包 0x28
     * LBS 地址请求包 0x17
     * 信息传输通用包 0x94
     * 中文地址回复包 0x17
     * 英文地址回复包 0x97
     */
    public static final byte PROTOCOL_GPS_LOCATION_REPLY = 0x2D;
    public static final byte PROTOCOL_GPS_ADDR_REQ = 0x2A;

    public static final byte PROTOCOL_WIFI_DATA = 0x2C;
    public static final byte PROTOCOL_LBS_ADDR_REQ = 0x17;
    public static final byte PROTOCOL_LBS_MULTI_STATION = 0x28;

    public static final byte PROTOCOL_ALERT_GPS_MULTI = 0x27;
    public static final byte PROTOCOL_ALERT_LBS = 0x19;

    public static final byte PROTOCOL_TIME_CHECK = (byte) 0x8A;
    public static final byte PROTOCOL_GENERAL_TRANSFER = (byte) 0x94;

    public static final byte PROTOCOL_ADDR_CN_REPLY = 0x17;
    public static final byte PROTOCOL_ADDR_EN_REPLY = (byte) 0x97;

}
