package me.seu.demo.service.netty.enums;

import me.seu.demo.service.netty.constants.ProtocolConstants;

/**
 * 消息类型
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/3/30 下午5:40
 */
public enum EventEnum {

    /**
     * 登录验证
     */
    LOGIN(ProtocolConstants.PROTOCOL_LOGIN),
    /**
     * 心跳
     */
    HEARTBEAT(ProtocolConstants.PROTOCOL_HEARTBEAT),
    /**
     * GPS 定位包
     */
    GPS_LOCATION(ProtocolConstants.PROTOCOL_GPS_LOCATION),
    /**
     * 报警数据GPS: 单围栏
     */
    ALERT_GPS(ProtocolConstants.PROTOCOL_ALERT_GPS),
    /**
     * 在线指令 下行通知
     */
    ONLINE_INSTRUCTION(ProtocolConstants.PROTOCOL_ONLINE_INSTRUCTION),
    /**
     * 终端在线指令回复
     */
    TERMINAL_ONLINE_INSTRUCTION_REPLY(ProtocolConstants.PROTOCOL_ONLINE_INSTRUCTION_REPLY),
    ;

    private byte v;

    private EventEnum(byte v) {
        this.v = v;
    }

    public short getVal() {
        return v;
    }

    public static EventEnum valuesOf(byte e) {
        EventEnum[] vs = EventEnum.values();
        if (vs == null || vs.length == 0) {
            return null;
        }
        for (EventEnum event : vs) {
            if (event.getVal() == e) {
                return event;
            }
        }
        return null;
    }

}
