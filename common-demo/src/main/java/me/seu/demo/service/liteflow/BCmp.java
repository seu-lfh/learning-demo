package me.seu.demo.service.liteflow;

import com.yomahub.liteflow.core.NodeComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * liteFlow 组件2
 *
 * @author liangfeihu
 * @since 2022/11/8 10:24
 */
@Slf4j
@Component("b")
public class BCmp extends NodeComponent {
    @Override
    public void process() throws Exception {
        log.info("liteFlow 组件2: 处理中~~~");
    }
}
