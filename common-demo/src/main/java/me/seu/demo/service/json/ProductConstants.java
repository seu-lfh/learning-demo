package me.seu.demo.service.json;

/**
 * 产品相关常量
 *
 * @author liangfeihu
 * @since 2021/5/20 上午10:43
 */
public interface ProductConstants {

    String PRODUCT_TSL_PREFIX = "product_tsl_";
    String PRODUCT_TSL_TIME = "product_tsl_time_";

    String PRODUCT_TSL_PROPERTY = "1";
    String PRODUCT_TSL_SERVICE = "2";
    String PRODUCT_TSL_EVENT = "3";
    Integer PRODUCT_TSL_PROPERTY_VALUE  = 1;
    Integer PRODUCT_TSL_SERVICE_VALUE  = 2;
    Integer PRODUCT_TSL_EVENT_VALUE  = 3;

    String CALL_TYPE_SYNC = "sync";
    Integer CALL_TYPE_SYNC_VALUE = 1;

    String CALL_TYPE_ASYNC = "async";
    Integer CALL_TYPE_ASYNC_VALUE = 2;

    String EVENT_TYPE_INFO = "info";
    Integer EVENT_TYPE_INFO_VALUE = 1;
    String EVENT_TYPE_ALARM = "alarm";
    Integer EVENT_TYPE_ALARM_VALUE = 2;
    String EVENT_TYPE_FAULT = "fault";
    Integer EVENT_TYPE_FAULT_VALUE = 3;

    String PRODUCT_TSL_JSON_FULL = "full";
    String PRODUCT_TSL_JSON_SIMPLE = "simple";

    Integer PRODUCT_TOPIC_TYPE_BASIC  = 1;
    Integer PRODUCT_TOPIC_TYPE_TSL  = 2;

}
