package me.seu.demo.service;

import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author liangfeihu
 * @since 2020/4/16 14:49
 */
@Slf4j
@Service
public class AccessLimitService {
    /**
     * 每秒只发出5个令牌
     */
    RateLimiter rateLimiter = RateLimiter.create(5.0);

    /**
     * 尝试获取令牌
     * 返回true表示获取到
     *
     * @return
     */
    public boolean tryAcquire() {
        return rateLimiter.tryAcquire();
    }

    public AccessLimitService() {
        log.info("[AccessLimitService] 无参构造函数");
    }

    public String limitStr = "test limit str";

    public String getLimitStr() {
        return limitStr + " method";
    }

    public void setLimitStr(String limitStr) {
        log.info("[AccessLimitService] setLimitStr method");
        this.limitStr = limitStr;
    }

}
