package me.seu.demo.service.netty.utils;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;

/**
 * 日期时间工具类
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/6 下午3:53
 */
@Slf4j
public class TimeUtils {

    public static byte[] getCurrentDateTime() {
        byte[] dt = new byte[6];
        DateTime dateTime = new DateTime();
        int year = dateTime.getYear();
        dt[0] = (byte)(0x000000FF & (year % 100));
        int month = dateTime.getMonthOfYear();
        dt[1] = (byte) (0x000000FF & month);
        int day = dateTime.getDayOfMonth();
        dt[2] = (byte) (0x000000FF & day);

        int hour = dateTime.getHourOfDay();
        dt[3] = (byte) (0x000000FF & hour);
        int minute = dateTime.getMinuteOfHour();
        dt[4] = (byte) (0x000000FF & minute);
        int second = dateTime.getSecondOfMinute();
        dt[5] = (byte) (0x000000FF & second);
        return dt;
    }

}
