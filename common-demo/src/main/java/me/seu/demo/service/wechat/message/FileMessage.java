package me.seu.demo.service.wechat.message;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件 消息
 *
 * @author liangfeihu
 * @since 2021/10/19 16:41
 */
public class FileMessage implements Message {

    private String type = "file";
    private String mediaId;

    public FileMessage(String mediaId) {
        this.mediaId = mediaId;
    }

    @Override
    public String toJsonString() {
        Map<String, Object> items = new HashMap();
        items.put("msgtype", "file");

        Map<String, String> file = new HashMap();
        if (StringUtils.isBlank(mediaId)) {
            throw new IllegalArgumentException("mediaId should not be blank");
        }
        file.put("media_id", mediaId);

        items.put("file", file);
        return JSON.toJSONString(items);
    }

}
