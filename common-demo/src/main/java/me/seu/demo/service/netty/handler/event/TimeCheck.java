package me.seu.demo.service.netty.handler.event;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.netty.handler.Process;
import me.seu.demo.service.netty.message.GpsMessage;
import me.seu.demo.service.netty.utils.NetUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

/**
 * 校时包处理 0x8A
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/6 下午3:39
 */
@Slf4j
public class TimeCheck implements Process {

    @Override
    public void execute(GpsMessage msg) throws Exception {
        log.info("[TimeCheck]reply time check packet {}", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        byte[] checkPacket = NetUtils.buildTimeCheckPacket(msg);
        ChannelHandlerContext ctx = msg.getCtx();
        if (ctx.channel().isWritable()) {
            // 将校时响应包发送出去
            ctx.pipeline().writeAndFlush(Unpooled.wrappedBuffer(checkPacket));
        } else {
            ctx.pipeline().close();
        }
    }

}
