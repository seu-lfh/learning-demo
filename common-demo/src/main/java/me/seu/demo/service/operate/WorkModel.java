package me.seu.demo.service.operate;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 计算结果
 *
 * @author liangfeihu
 * @since 2022/10/11 17:52
 */
@Data
@NoArgsConstructor
public class WorkModel {

    /**
     * 开工阈值
     */
    private Integer workThreshold;

    /**
     * 最小稳定段的波动值
     */
    private Integer maxMinDiff;

    /**
     * 选择的稳定区间
     */
    private Point stablePoint;

    /**
     * 波动阈值
     */
    private BigDecimal stop;

}
