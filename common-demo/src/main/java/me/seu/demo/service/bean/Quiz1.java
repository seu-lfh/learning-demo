package me.seu.demo.service.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

/**
 * 内部类Bean注入
 *
 * @author liangfeihu
 * @since 2021/10/7 20:10
 */
@Controller
public class Quiz1 {

    @Autowired
    @Qualifier("quiz1.QUIZInnerClass")
    QUIZInnerClass innerClassDataService;

    @PostConstruct
    public void init(){
        innerClassDataService.test();
    }

    @Component
    public static class QUIZInnerClass{
        public void test() {
            System.out.println("quiz1 test");
        }
    }
}
