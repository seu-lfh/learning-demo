package me.seu.demo.service.netty.constants;

import io.netty.util.AttributeKey;

/**
 * SysConst
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/3/30 下午5:22
 */
public class SysConstants {

    /**
     * 安全验证key
     */
    public static final AttributeKey<String> SECURE_KEY = AttributeKey.valueOf("SECURE_KEY");
    /**
     * 终端标识：DEVICE_ID
     */
    public static final AttributeKey<String> DEVICE_ID = AttributeKey.valueOf("DEVICE_ID");
    /**
     * wifi模块版本key
     */
    public static final AttributeKey<String> WIFI_VER_KEY = AttributeKey.valueOf("WIFI_VER_KEY");
    /**
     * mcu模块版本key
     */
    public static final AttributeKey<String> MCU_VER_KEY = AttributeKey.valueOf("MCU_VER_KEY");

}
