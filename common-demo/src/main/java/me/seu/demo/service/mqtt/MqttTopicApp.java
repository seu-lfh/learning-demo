package me.seu.demo.service.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * mqtt topic 测试案例
 *
 * @author liangfeihu
 * @since 2021/11/4 10:36
 */
public class MqttTopicApp {

    public static void main(String[] args) {
        String productKey = "a1r89yeIGqU";
        String deviceName = "device_01";
        String deviceSecret = "28f058e8e7fe2ba3652c42a3aea3a786";

        //计算Mqtt建联参数
        MqttSign sign = new MqttSign();
        sign.calculate(productKey, deviceName, deviceSecret);

        System.out.println("username: " + sign.getUsername());
        System.out.println("password: " + sign.getPassword());
        System.out.println("clientid: " + sign.getClientid());

        System.out.println("----------------------------------");
        //使用Paho连接阿里云物联网平台
        String port = "443";
        String broker = "ssl://" + productKey + ".iot-as-mqtt.cn-shanghai.aliyuncs.com" + ":" + port;
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            //Paho Mqtt 客户端
            MqttClient sampleClient = new MqttClient(broker, sign.getClientid(), persistence);

            //Paho Mqtt 连接参数
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setKeepAliveInterval(180);
            connOpts.setUserName(sign.getUsername());
            connOpts.setPassword(sign.getPassword().toCharArray());
            sampleClient.connect(connOpts);
            System.out.println("broker: " + broker + " Connected");

            //Paho Mqtt 消息订阅
            String topicReply = "/" + productKey + "/" + deviceName + "/user/get";
            sampleClient.subscribe(topicReply, new Mqtt3PostPropertyMessageListener());
            System.out.println("subscribe: topic=" + topicReply);

            //Paho Mqtt 消息发布
            String topic = "/" + productKey + "/" + deviceName + "/user/post/data";
            String content = "{\"id\":\"1\",\"version\":\"1.0\",\"params\":{\"LightSwitch\":1}}";
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(0);
            sampleClient.publish(topic, message);
            System.out.println("publish: topic=" + topic + " content=" + content);

            System.out.println("----------------------------------");
            Thread.sleep(50 * 2000);

            //Paho Mqtt 断开连接
            sampleClient.disconnect();
            System.out.println("----------------------------------");
            System.out.println("Disconnected");
            System.exit(0);
        } catch (MqttException e) {
            System.out.println("reason " + e.getReasonCode());
            System.out.println("msg " + e.getMessage());
            System.out.println("loc " + e.getLocalizedMessage());
            System.out.println("cause " + e.getCause());
            System.out.println("excep " + e);
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
