package me.seu.demo.service.emqx;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * mqtt consumer 2
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/28 下午4:26
 */
public class OnMessageCallback2 implements MqttCallbackExtended {

    @Override
    public void connectComplete(boolean reconnect, String serverUrl) {
        System.out.println("mqtt连接成功：" + serverUrl + " reconnect flag=" + reconnect);
    }


    @Override
    public void connectionLost(Throwable cause) {
        // 连接丢失后，一般在这里面进行重连
        System.out.println("连接断开，可以做重连");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        // subscribe后得到的消息会执行到这里面
        System.out.println(Thread.currentThread().getName() + " 接收消息主题:" + topic);
        System.out.println(Thread.currentThread().getName() + " 接收消息Qos:" + message.getQos());
        System.out.println(Thread.currentThread().getName() + " 接收消息内容:" + new String(message.getPayload()));
        System.out.println(" -----------------------------------------------");
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println(Thread.currentThread().getName() + " deliveryComplete---------" + token.isComplete());
    }

}
