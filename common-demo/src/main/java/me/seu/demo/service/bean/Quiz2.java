package me.seu.demo.service.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

/**
 * 注入顺序依次为：1. 构造器注入 2. 类属性注入 3. 类方法注入
 *
 * @author liangfeihu
 * @since 2021/10/7 19:49
 */
@Controller
public class Quiz2 {
    @Autowired
    Student student;

    @PostConstruct
    public void init() {
        System.out.println(student.a);
    }

    public Quiz2(Student student) {
        student.a = 2;
    }

    @Autowired
    public void setStudent(Student student) {
        student.a = 3;
    }

    @Component
    public static class Student {
        public int a = 1;
    }
}

