package me.seu.demo.service.liteflow;

import com.alibaba.fastjson.JSONObject;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 第一个执行引擎
 *
 * @author liangfeihu
 * @since 2022/11/8 10:33
 */
@Slf4j
@Component
public class FirstFlowExecutor {

    @Resource
    private FlowExecutor flowExecutor;

    public void testConfig(){
        LiteflowResponse response = flowExecutor.execute2Resp("chain1", "arg");
        log.info("[FirstFlowExecutor] testConfig resp = {}", JSONObject.toJSONString(response));
    }

}
