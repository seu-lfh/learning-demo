package me.seu.demo.service.netty.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;

/**
 * 自定义帧编码器
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/3/30 下午5:20
 */
@Slf4j
public class TransferEncoder extends MessageToByteEncoder<Object> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out) throws Exception {
        // 无需再编码
        // 业务端直接构造字节数组，发送出去
        log.info("[TransferEncoder] do nothing ~");
    }

}
