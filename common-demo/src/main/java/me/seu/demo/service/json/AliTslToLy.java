package me.seu.demo.service.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.formula.functions.T;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 阿里物模型转化同程物模型
 *
 * @author liangfeihu
 * @since 2021/6/23 10:39
 */
public class AliTslToLy {

    public static void main(String[] args) throws Exception {
        String jsonFilePath = "/Users/a123/codes/myown/learning-demo/common-demo/src/main/resources/json/congji.json";
        File file = new File(jsonFilePath );
        String inputJson = FileUtils.readFileToString(file,"UTF-8");
        System.out.println(inputJson);

        Tsl tsl = new Tsl();
        JSONObject jsonObject = JSONObject.parseObject(inputJson);
        Tsl.Profile profile = jsonObject.getObject("profile", Tsl.Profile.class);
        tsl.setProfile(profile);

        List<Tsl.Property> propertyList = new ArrayList<>();
        JSONArray properties = jsonObject.getJSONArray("properties");
        if (properties != null && properties.size() > 0) {
            for (int i = 0; i < properties.size(); i++) {
                JSONObject propertyObject = properties.getJSONObject(i);
                String identifier = propertyObject.getString("identifier");
                String name = propertyObject.getString("name");
                String desc = propertyObject.getString("desc");
                String accessMode = propertyObject.getString("accessMode");
                Boolean required = propertyObject.getBoolean("required");
                Tsl.Property property = new Tsl.Property(identifier, name, accessMode, required, desc);

                JSONObject dataType = propertyObject.getJSONObject("dataType");
                String type = dataType.getString("type");
                property.setDataType(type);
                if ("enum".equals(type) || "struct".equals(type)) {
                    property.setDataSpecs(dataType.getJSONArray("specs"));
                } else {
                    property.setDataSpecs(dataType.getJSONObject("specs"));
                }
                propertyList.add(property);
            }
        }
        tsl.setProperties(propertyList);

        // 事件处理
        JSONArray eventArray = jsonObject.getJSONArray("events");
        // 物模型事件解析
        List<Tsl.Event> events = getTslEventsList(eventArray);
        tsl.setEvents(events);

        // 服务处理
        JSONArray serviceArray = jsonObject.getJSONArray("services");
        // 物模型服务解析
        List<Tsl.Service> services = getTslServiceList(serviceArray);
        tsl.setServices(services);

        System.out.println(tsl.toString());

    }

    private static List<Tsl.Service> getTslServiceList(JSONArray services) {
        List<Tsl.Service> serviceList = new ArrayList<>();
        if (services != null && services.size() > 0) {
            for (int i = 0; i < services.size(); i++) {
                JSONObject jsonObject = services.getJSONObject(i);
                String identifier = jsonObject.getString("identifier");
                String callType = jsonObject.getString("callType");
                JSONArray inputData = jsonObject.getJSONArray("inputData");
                JSONArray outputData = jsonObject.getJSONArray("outputData");

                Tsl.Service service = new Tsl.Service(identifier, jsonObject.getString("name"),
                        jsonObject.getString("method"), getCallType(callType), inputData, outputData,
                        jsonObject.getBoolean("required"), jsonObject.getString("desc")
                );

                serviceList.add(service);
            } // end for
        } // end if
        return serviceList;
    }

    public static List<Tsl.Event> getTslEventsList(JSONArray events) {
        List<Tsl.Event> eventList = new ArrayList<>();
        if (events != null && events.size() > 0) {
            for (int i = 0; i < events.size(); i++) {
                JSONObject jsonObject = events.getJSONObject(i);
                String identifier = jsonObject.getString("identifier");
                String eventType = jsonObject.getString("type");

                JSONArray outputData = jsonObject.getJSONArray("outputData");
                Tsl.Event event = new Tsl.Event(identifier, jsonObject.getString("name"),
                        jsonObject.getString("method"), getEventType(eventType), outputData,
                        jsonObject.getBoolean("required"), jsonObject.getString("desc")
                );
                eventList.add(event);
            }
        }
        return eventList;
    }

    private static Integer getEventType(String eventType)  {
        if (ProductConstants.EVENT_TYPE_INFO.equals(eventType)) {
            return ProductConstants.EVENT_TYPE_INFO_VALUE;
        } else if (ProductConstants.EVENT_TYPE_ALARM.equals(eventType)) {
            return ProductConstants.EVENT_TYPE_ALARM_VALUE;
        } else if (ProductConstants.EVENT_TYPE_FAULT.equals(eventType)) {
            return ProductConstants.EVENT_TYPE_FAULT_VALUE;
        }
        return 1;
    }

    private static Integer getCallType(String callType)  {
        if (ProductConstants.CALL_TYPE_ASYNC.equals(callType)) {
            return ProductConstants.CALL_TYPE_ASYNC_VALUE;
        } else if (ProductConstants.CALL_TYPE_SYNC.equals(callType)) {
            return ProductConstants.CALL_TYPE_SYNC_VALUE;
        }
        return 2;
    }

}
