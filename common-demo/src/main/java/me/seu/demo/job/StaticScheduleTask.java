package me.seu.demo.job;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 定时任务
 * 自定义定时任务线程池{@link me.seu.demo.config.SchedulerConfig}
 *
 * @author liangfeihu
 * @since 2020/4/26 18:12
 */
@Slf4j
@Component
public class StaticScheduleTask {

    //@Scheduled(cron = "0/2 * * * * ?")
    public void taskOne() {
        log.info("[{}] 0/2 * * * * ? time={}", Thread.currentThread().getName(), DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        try {
            int nextInt = RandomUtils.nextInt(0, 3);
            Thread.sleep(nextInt * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String numeric = RandomStringUtils.randomNumeric(6);
    }

    //@Scheduled(cron = "0/5 * * * * ?")
    public void taskTwo() {
        log.info("[{}] 0/5 * * * * ? time={}", Thread.currentThread().getName(), DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
