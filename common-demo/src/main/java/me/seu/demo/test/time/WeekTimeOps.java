package me.seu.demo.test.time;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 日期操作类
 *
 * @author: liangfeihu
 * @since: 2024-11-12 19:31
 */
public class WeekTimeOps {

    public static void main(String[] args) {
        DateTime dateTime = DateUtil.lastWeek();
        // 获取上一周的开始时间（周一）
        Date startOfLastWeek = DateUtil.beginOfWeek(dateTime);
        // 获取上一周的结束时间（周日）
        Date endOfLastWeek = DateUtil.endOfWeek(dateTime);

        // 打印结果
        System.out.println("上一周开始时间：" + DateUtil.formatDateTime(startOfLastWeek));
        System.out.println("上一周结束时间：" + DateUtil.formatDateTime(endOfLastWeek));

        System.out.println(DateUtil.weekOfYear(dateTime));

        DateTime dateTime1 = DateUtil.offsetWeek(startOfLastWeek, -1);
        DateTime dateTime2 = DateUtil.offsetWeek(endOfLastWeek, -1);

        // 打印结果
        System.out.println("上上一周开始时间：" + DateUtil.formatDateTime(dateTime1));
        System.out.println("上上一周结束时间：" + DateUtil.formatDateTime(dateTime2));

        System.out.println("-----------------------------------------");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        System.out.println(now.minusWeeks(1));
        System.out.println(now.getDayOfWeek().getValue());
        System.out.println(DateUtil.format(now, "yyyy_MM_dd"));
        System.out.println(DateUtil.format(now, "HH_mm_ss"));
    }

}
