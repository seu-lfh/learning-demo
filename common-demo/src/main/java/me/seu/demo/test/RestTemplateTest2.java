package me.seu.demo.test;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * @author liangfeihu
 * @since 2020/6/17 17:49
 */
public class RestTemplateTest2 {

    public static void main2(String[] args) {
        String url = "https://hcp-prod.leapstack.cn/gw/num/login";

        RestTemplate restTemplate = new RestTemplate();

        String requestBody = "{\"password\":\"Aa123456\",\"username\":\"yiankefu\"}";

        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> httpEntity = new HttpEntity<String>(requestBody, headers);

        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
        System.out.println(stringResponseEntity.getBody());
    }

    public static void main3(String[] args) {
        RestTemplate restTemplate = new RestTemplate();

        String tokenUrl = "https://open.ys7.com/api/lapp/token/get";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        // 封装参数，千万不要替换为Map与HashMap，否则参数无法传递
        MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        params.add("appKey", "4dba6ce432d1469488bea099d89419f4");
        params.add("appSecret", "0dd50f6a6fb548b35061a132e2303a9d");
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(params, headers);
        // 执行HTTP请求
        ResponseEntity<JSONObject> response = restTemplate.exchange(tokenUrl, HttpMethod.POST, requestEntity, JSONObject.class);
        // 输出结果
        JSONObject body = response.getBody();
        System.out.println(response.getStatusCodeValue() + " body=" + body.toJSONString());
        System.out.println(body.getIntValue("code") + " ：" + body.getString("msg"));
        System.out.println(body.getJSONObject("data").toJSONString());
    }

    public static void main4(String[] args) {
        RestTemplate restTemplate = new RestTemplate();

        String tokenUrl = "https://open.hikyun.com/artemis/oauth/token/v2";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject params = new JSONObject();
        params.put("accessKey", "16526693828673480");
        params.put("secretKey", "mprxlJugrurHQUQKdrZV");
        params.put("productCode", "1652671401354364");
        params.put("projectId", "2015529116881008");

        HttpEntity<JSONObject> requestEntity = new HttpEntity<>(params, headers);
        // 执行HTTP请求
        ResponseEntity<JSONObject> response = restTemplate.postForEntity(tokenUrl, requestEntity, JSONObject.class);
        // 输出结果
        JSONObject body = response.getBody();
        System.out.println(response.getStatusCodeValue() + " body=" + body.toJSONString());
        System.out.println(body.getIntValue("code") + " ：" + body.getString("msg"));
        System.out.println(body.getJSONObject("data").toJSONString());
    }

    public static void main5(String[] args) {
        RestTemplate restTemplate = new RestTemplate();

        String tokenUrl = "https://open.hikyun.com/artemis/api/eits/v2/global/device/delete";

        HttpHeaders headers = new HttpHeaders();
        headers.add("access_token", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhY2Nlc3NfdG9rZW4iLCJwYXlsb2FkIjoie1wiYWNjZXNzS2V5XCI6XCIxNjUyNjY5MzgyODY3MzQ4MFwiLFwiZXhwaXJlZFwiOjQzMjAwLFwicHJvZHVjdENvZGVcIjpcIjE2NTI2NzE0MDEzNTQzNjRcIixcInByb2plY3RJZFwiOjIwMTU1MjkxMTY4ODEwMDh9IiwiZXhwIjoxNjUzNjg0NDA4fQ.x4QiVsCHJAz32iPLr8N52tohVfidV0bYu0ggIK7xfwAKGHjSg-R8x7VQYZxuc4bhrlUf3nY3omXMg3LZcqq4lA");
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject params = new JSONObject();
        params.put("projectId", "2015529116881008");
        params.put("deviceSerials", Arrays.asList("J67525437"));

        HttpEntity<JSONObject> requestEntity = new HttpEntity<>(params, headers);
        // 执行HTTP请求
        ResponseEntity<JSONObject> response = restTemplate.postForEntity(tokenUrl, requestEntity, JSONObject.class);
        // 输出结果
        JSONObject body = response.getBody();
        System.out.println(response.getStatusCodeValue() + " body=" + body.toJSONString());
        System.out.println(body.getIntValue("code") + " ：" + body.getString("msg"));
        System.out.println(body.getJSONObject("data").toJSONString());
    }

}
