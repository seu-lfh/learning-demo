package me.seu.demo.test;

public class MatrixTest {

    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {1, 4, 7, 11, 15},
                {2, 5, 8, 12, 19},
                {3, 6, 9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30}
        };
        int target = 5;
        System.out.println(checkMatrixHasNumV2(matrix, matrix.length, matrix[0].length, target));
        target = 20;
        System.out.println(checkMatrixHasNumV2(matrix, matrix.length, matrix[0].length, target));
    }

    public static boolean checkMatrixHasNum(int[][] matrix, int n, int m, int num) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (matrix[i][j] == num) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkMatrixHasNumV2(int[][] matrix, int n, int m, int num) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (matrix[i][j] == num) {
                    return true;
                } else if(matrix[i][j] > num) {
                    break;
                }
            }
        }
        return false;
    }

}
