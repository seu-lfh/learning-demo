package me.seu.demo.test.json;

import com.alibaba.fastjson.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.common.msg.BaseExchangeMessage;
import me.seu.demo.common.msg.MqttExchangeMessage;
import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * msg 消息
 *
 * @author liangfeihu
 * @since 2022/11/4 14:12
 */
@Slf4j
public class MsgForString {

    public static void main2(String[] args) {
        String formatTime = "yyyy-MM-dd HH:mm:ss";

        BaseExchangeMessage baseMessage = new BaseExchangeMessage();
        baseMessage.setPayload("base message");
        log.info("baseMessage info = {}", JSONObject.toJSONString(baseMessage));
        log.info("baseMessage time = {}",
                DateFormatUtils.format(baseMessage.getReportTime(), formatTime));

        MqttExchangeMessage mqttMessage = new MqttExchangeMessage();
        mqttMessage.setTopic("/abc/123/event/op_report/post");
        mqttMessage.setPayload("mqtt message");
        log.info("mqttMessage info = {}", JSONObject.toJSONString(mqttMessage));
        log.info("mqttMessage time = {}",
                DateFormatUtils.format(mqttMessage.getReportTime(), formatTime));

    }

    public static void main3(String[] args) {
        BaseExchangeMessage baseMessage1 = new BaseExchangeMessage();
        baseMessage1.setPayload("message1");
        BaseExchangeMessage baseMessage2 = new BaseExchangeMessage();
        baseMessage2.setPayload("message2");
        BaseExchangeMessage baseMessage3 = new BaseExchangeMessage();
        baseMessage3.setPayload("message3");

        List<BaseExchangeMessage> list = new ArrayList<>();
        list.add(baseMessage1);
        list.add(baseMessage2);
        list.add(baseMessage3);

        String collect = list.stream().map(BaseExchangeMessage::getPayload)
                .collect(Collectors.joining(","));
        System.out.println(collect);
    }

    public static void main5(String[] args) {
        String str1 = "message1,message2,message3";
        String[] split = str1.split(",");
        System.out.println(Arrays.toString(split));

        System.out.println("----------------------------");

        String str2 = "message1";
        String[] split2 = str2.split(",");
        System.out.println(Arrays.toString(split2));
    }

    public static void main(String[] args) {
        //System.out.println(66L * 100 / 107L + "%");
        //System.out.println(76L * 100 / 107L + "%");
        //System.out.println(96L * 100 / 107L + "%");
        //System.out.println(0 / 0 + "%");
        System.out.println("模板code，"
                + "20002：建筑模版，"
                + "20003：火灾报警系统模板，"
                + "20004：安全生产责任书模板，"
                + "20005：动火证模版，"
                + "20006：检查记录模版，"
                + "20007：隐患记录模版，"
                + "20008：消防评估模版，"
                + "20009：政校联动模版，"
                + "20010：微型消防站成员模板,"
                + "20011：设备维修记录模板,"
                + "20012：消火栓系统导入模板,"
                + "20013：消防管理人员模板,"
                + "20014：消防维保人员模板,"
                + "20015：物业内保人员模板,"
                + "20016：应急安保人员模板,"
                + "20017: 自动喷水灭火系统导入模板,"
                + "20018: 气体灭火系统导入模板,"
                + "20019: 防排烟系统导入模板,"
                + "20020: 防火门监控系统导入模板,"
                + "20021: 防自动消防水炮系统导入模板,"
                + "20022: 应急照明系统导入模板,"
                + "20023: 消防设备电源监控系统导入模板,"
                + "20024: 消防器材导入模板,"
                + "20025: 消防标识标牌导入模板,"
                + "20026: 物联设备导入模板,"
                + "20027: 监控设备导入模板");
    }

}
