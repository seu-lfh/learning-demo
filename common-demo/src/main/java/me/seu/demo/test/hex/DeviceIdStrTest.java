package me.seu.demo.test.hex;

import me.seu.demo.service.netty.utils.ByteUtils;

import java.util.Arrays;

/**
 * DeviceIdStrTest
 *
 * @author liangfeihu
 * @since 2021/8/4 14:17
 */
public class DeviceIdStrTest {

    /**
     * 获取deviceId
     */
    public static String getDeviceIdFromByte(byte[] content) throws Exception {
        if (content == null || content.length <= 8) {
            throw new Exception("获取deviceId,数据长度错误");
        }
        // 例：IMEI 号为 123456789123456，则终端 ID 为：0x01 0x23 0x45 0x67 0x89 0x12 0x34 0x56
        byte[] bytesNew = Arrays.copyOf(content, 8);
        String deviceId = ByteUtils.bytesToHexString(bytesNew);

        StringBuilder builder = new StringBuilder(deviceId);
        while ('0' == builder.charAt(0)) {
            builder.deleteCharAt(0);
        }
        return builder.toString();
    }

    public static void main(String[] args) throws Exception{
        byte[] deviceIds = new byte[] {0x11, 0x23, 0x45, 0x67, (byte) 0x89, 0x12, 0x34, 0x56, 0x00};
        String deviceId = getDeviceIdFromByte(deviceIds);
        System.out.println(deviceId);
    }


}
