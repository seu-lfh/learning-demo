package me.seu.demo.test.json;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.text.StringEscapeUtils;

/**
 * FastJson 字符串转义
 *
 * @author liangfeihu
 * @since 2021/6/25 19:00
 */
public class FastJson {

    public static void main(String[] args) {
        String jsonStr = "{attribute: \"ver\", pattern: \"<\", compareValue: \"67\"}";
        JSONObject object = JSONObject.parseObject(jsonStr);
        System.out.println(object.toJSONString());

        String params= "{\"ruleDesc\":\"自动开空调\",\"ruleId\":1,\"ruleName\":\"自动开空调\",\"triggerActions\":[{\"actionType\":1,\"deviceList\":[\"comm_device_test_0001\"],\"identifier\":\"mqttConnectPort\",\"inputParams\":{\"identifier\":\"mqttConnectPort\",\"inputValue\":\"6565\"},\"productKey\":\"nMktPGE01Nek\",\"tslName\":\"MQTT协议通信连接端口\"}],\"triggerItems\":[{\"allFlag\":true,\"deviceList\":[],\"productKey\":\"eoCBxAknlogX\",\"triggerDetail\":{\"output\":\"lat\",\"compareValue\":\"7676\",\"pattern\":\"&gt;=\",\"event\":\"activate_succ\"},\"triggerType\":3},{\"allFlag\":true,\"deviceList\":[],\"productKey\":\"eoCBxAknlogX\",\"triggerDetail\":{\"compareValue\":\"67\",\"pattern\":\"=\",\"attribute\":\"ver\"},\"triggerType\":1},{\"allFlag\":false,\"triggerDetail\":{}}],\"triggerMode\":1}\n" +
                "2021-06-25 18:55:21.219 INFO  [] [TID:d4722b5a827d442aa3d9e2efc932cbba.115.16246185212130743] [] [http-nio-8080-exec-7] [SceneRuleController] [editRule] params={\"ruleDesc\":\"自动开空调\",\"ruleId\":1,\"ruleName\":\"自动开空调\",\"triggerActions\":[{\"actionType\":1,\"deviceList\":[\"comm_device_test_0001\"],\"identifier\":\"mqttConnectPort\",\"inputParams\":{\"identifier\":\"mqttConnectPort\",\"inputValue\":\"6565\"},\"productKey\":\"nMktPGE01Nek\",\"tslName\":\"MQTT协议通信连接端口\"}],\"triggerItems\":[{\"allFlag\":true,\"deviceList\":[],\"productKey\":\"eoCBxAknlogX\",\"triggerDetail\":{\"output\":\"lat\",\"compareValue\":\"7676\",\"pattern\":\"&lt;=\",\"event\":\"activate_succ\"},\"triggerType\":3},{\"allFlag\":true,\"deviceList\":[],\"productKey\":\"eoCBxAknlogX\",\"triggerDetail\":{\"compareValue\":\"67\",\"pattern\":\"&lt;\",\"attribute\":\"ver\"},\"triggerType\":1}],\"triggerMode\":1}";
        String s = StringEscapeUtils.unescapeHtml4(params);
        System.out.println(s);
        String s1 = StringEscapeUtils.escapeHtml4(jsonStr);
        System.out.println(s1);
        String s2 = StringEscapeUtils.unescapeHtml4(s1);
        System.out.println(s2);

        String hello = "{\"temperature\":\"1\"}";
        System.out.println(hello);
        System.out.println(StringEscapeUtils.unescapeHtml4(hello));
    }

}
