package me.seu.demo.test;

import com.google.common.collect.Sets;

/**
 * @author liangfeihu
 * @since 2020/1/6 18:16
 */
public class DoubleAndString {
    public static void main(String[] args) {
        String relPay = "101.25";
        System.out.println(Double.valueOf(relPay));
        Double pay = Double.valueOf(relPay);
        Double zero = 200d;
        System.out.println(pay.compareTo(zero));
        Double value = Double.valueOf("51.03");
        System.out.println(pay - value);

        System.out.println(formatDouble("80.3"));
        System.out.println(formatDouble("85.3"));
        System.out.println(formatDouble("82.8"));

        System.out.println("---------------");
        System.out.println(calDouble() + "");

        Sets.newHashSet("I10.x05 \u0001 高血压Ⅲ期".split(","))
                .forEach(illCodeStr -> {
                    String[] illStr = illCodeStr.split("\\s+");
                    if (illStr.length > 0) {
                        System.out.println(illStr[0]);
                    }
                    if (illStr.length > 1) {
                        System.out.println(illStr[1]);
                    }
                });

        double billSum = 0.00d;
        System.out.println(billSum == 0);
    }

    public static String formatDouble(String dVal) {
        Double val = Double.valueOf(dVal);
        double v = val / 100;
        String format = String.format("%.2f", v);
        return format;
    }

    public static Double calDouble() {
        Double pay = 0D;
        pay += 3.06D;
        pay += 2.163D;

        return pay;
    }
}
