package me.seu.demo.test.time;

import cn.hutool.core.date.DateTime;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.common.AppBizException;
import me.seu.demo.common.ResultCode;
import me.seu.demo.utils.AppBizExceptionUtil;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * DateTimeOne
 *
 * @author liangfeihu
 * @since 2021/6/26 15:52
 */
@Slf4j
public class DateTimeOne {

    public static void main(String[] args) throws Exception {
        String week_array = "1/2/3/4/5/6/7";
        Date startDate = DateUtils.parseDate("20230118", "yyyyMMdd");
        Date endDate = DateUtils.parseDate("20230120", "yyyyMMdd");
        List<String> weekList = new ArrayList<>();
        while (startDate.getTime() <= endDate.getTime()) {
            weekList.add(getWeekStr(startDate));
            startDate = DateUtils.addDays(startDate, 1);
        }
        boolean validFlag = false;
        String[] split = week_array.split("/");
        for (String weekStr : split) {
            if (weekList.contains(weekStr)) {
                validFlag = true;
                break;
            }
        }

        System.out.println(JSON.toJSONString(weekList));
        System.out.println(JSON.toJSONString(split));
        if (!validFlag) {
            System.out.println("-------error----------");
            throw new AppBizException("22011", "本条数据的旅行日期 与 航段" + 1 + "_旅行日期 有差异。生成商品时仅生成交集部分，请确认");
        }
        System.out.println("-------end----------");
    }
    private static  String getWeekStr(Date date) {
        DateTime dateTime = new DateTime(date);
        int week = dateTime.dayOfWeek();
        if (week == 1) {
            week = 7;
        } else {
            week = week - 1;
        }
        return week + "";
    }
    public static void main8(String[] args) throws Exception {
        Date dateStart = DateUtils.parseDate("20221121", "yyyyMMdd");
        Date dateEnd = DateUtils.parseDate("20221127", "yyyyMMdd");
        List<String> weekList = new ArrayList<>();
        while (dateStart.getTime() <= dateEnd.getTime()) {
            DateTime dateTime = new DateTime(dateStart);
            int week = dateTime.dayOfWeek();
            if (week == 1) {
                week = 7;
            } else {
                week = week - 1;
            }
            weekList.add(week + "");

            dateStart = DateUtils.addDays(dateStart, 1);
        }
        System.out.println(weekList);
    }

    public static void main3(String[] args) throws Exception {
        String dateStr = "20221201<20251231";//"20221130<20221202";
        validateTravelDate(dateStr);
        System.out.println("-------------");
        String[] split = dateStr.split("<");
        if (split.length != 2) {
            System.out.println(split.length);
        }
        System.out.println(split.length);
    }

    private static void validateTravelDate(String travelDate) throws AppBizException {
        if (travelDate.contains(",")) {
            String[] dateArr = travelDate.split(",");
            for (String dateStr : dateArr) {
                handleSingleDateStr(dateStr);
            }
        } else {
            handleSingleDateStr(travelDate);
        }
    }

    private static void handleSingleDateStr(String dateStr) throws AppBizException {
        Date nowDate = new Date();
        Date currDate = DateUtils.truncate(nowDate, Calendar.DAY_OF_MONTH);
        if (dateStr.contains("-")) {
            String[] split = dateStr.split("-");
            if (split.length != 2) {
                throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_INVALID);
            }
            try {
                Date startDate = DateUtils.parseDate(split[0], "yyyyMMdd");
                Date endDate = DateUtils.parseDate(split[1], "yyyyMMdd");
                long dayStep = (endDate.getTime() - startDate.getTime()) / (24 * 3600 * 1000);
                if (dayStep >= 361) {
                    throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_STEP_TOO_LONG);
                }
                if (startDate.getTime() < currDate.getTime()) {
                    throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_GE_TODAY);
                }
                if (endDate.getTime() < currDate.getTime()) {
                    throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_GE_TODAY);
                }
            } catch (Exception e) {
                throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_INVALID);
            }
        } else if (dateStr.contains("<")) {
            String[] split = dateStr.split("<");
            if (split.length != 2) {
                throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_INVALID);
            }
            try {
                Date startDate = DateUtils.parseDate(split[0], "yyyyMMdd");
                Date endDate = DateUtils.parseDate(split[1], "yyyyMMdd");
                if (endDate.getTime() < startDate.getTime()) {
                    throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_START_END_INVALID);
                }
                if (startDate.getTime() < currDate.getTime()) {
                    throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_GE_TODAY);
                }
                long dayStep = (endDate.getTime() - startDate.getTime()) / (24 * 3600 * 1000);
                if (dayStep >= 361) {
                    throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_STEP_TOO_LONG);
                }

            } catch (Exception e) {
                throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_INVALID);
            }
        } else {
            try {
                Date date = DateUtils.parseDate(dateStr, "yyyyMMdd");
                if (date.getTime() < currDate.getTime()) {
                    throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_GE_TODAY);
                }
            } catch (Exception e) {
                throw AppBizExceptionUtil.build(ResultCode.TASK_TRAVEL_DATE_INVALID);
            }
        }
    }

    public static void main1(String[] args) throws Exception {
        Date nowDate = new Date();
        System.out.println(DateFormatUtils.format(nowDate, "yyyy-MM-dd HH:mm:ss"));
        System.out.println(DateFormatUtils.format(nowDate, "yyyy-MM-dd"));
        System.out.println(DateFormatUtils.format(nowDate, "HH:mm:ss"));
        System.out.println("------------------------");

        String time = "2022-09-15T16:07:13";
        Date date = DateUtils.parseDate(time, "yyyy-MM-dd'T'HH:mm:ss");
        System.out.println(DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss"));
        System.out.println("------------------------");
        String time2 = "2022-09-16T17:08:39";
        Date parseDate = DateUtils.parseDate(time2.replace("T", " "), "yyyy-MM-dd HH:mm:ss");
        System.out.println(DateFormatUtils.format(parseDate, "yyyy-MM-dd HH:mm:ss"));

    }

    public static void main2(String[] args) {
        Date nowDate = new Date();
        System.out.println(DateFormatUtils.format(nowDate, "yyyy-MM-dd HH:mm:ss"));
        Date sDate = DateUtils.truncate(nowDate, Calendar.SECOND);
        Date mDate = DateUtils.truncate(nowDate, Calendar.MINUTE);
        Date hDate = DateUtils.truncate(nowDate, Calendar.HOUR);
        Date h2Date = DateUtils.truncate(nowDate, Calendar.HOUR_OF_DAY);
        System.out.println(DateFormatUtils.format(sDate, "yyyy-MM-dd HH:mm:ss"));
        System.out.println(DateFormatUtils.format(mDate, "yyyy-MM-dd HH:mm:ss"));
        System.out.println(DateFormatUtils.format(hDate, "yyyy-MM-dd HH:mm:ss"));
        System.out.println(DateFormatUtils.format(h2Date, "yyyy-MM-dd HH:mm:ss"));

        List<String> list = JSON.parseArray("[\"comm_device_test_0001\",\"comm_device_test_0002\"]", String.class);
        System.out.println(list.toString());
    }

}
