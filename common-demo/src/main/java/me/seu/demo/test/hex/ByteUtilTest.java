package me.seu.demo.test.hex;

import java.util.Arrays;

/**
 * Byte2Hex Util Test
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/3/31 下午2:56
 */
public class ByteUtilTest {

    public static String byteToHex(byte[] buffer) {
        return byteToHex(buffer, true);
    }

    public static String byteToHex(byte[] buffer, boolean prefix) {
        StringBuilder sb = new StringBuilder(128);
        if (prefix) {
            sb.append("0x");
        }
        for (int i = 0; i < buffer.length; i++) {
            String hv = Integer.toHexString(buffer[i] & 0xFF).toUpperCase();
            if (hv.length() < 2) {
                sb.append("0");
            }
            sb.append(hv);
        }
        return sb.toString();
    }

    public static int bytesToInteger(byte[] bytes) {
        return (bytes[0] & 0xFF) << 24 |
                (bytes[1] & 0xFF) << 16 |
                (bytes[2] & 0xFF) << 8 |
                (bytes[3] & 0xFF);
    }

    /**
     * 计算CheckSum
     */
    public static int checksum(byte[] data, int len) {
        int sum = 0;
        for (int j = 0; len > 1; len--) {
            sum += data[j++] & 0xff;
            if ((sum & 0x80000000) > 0) {
                //使用1的补码
                sum = (sum & 0xffff) + (sum >> 16);
            }
        }
        if (len == 1) {
            sum += data[data.length - 1] & 0xff;
        }
        while ((sum >> 16) > 0) {
            sum = (sum & 0xffff) + sum >> 16;
        }
        sum = (sum == 0xffff) ? sum & 0xffff : (~sum) & 0xffff;
        return sum;
    }

    public static int crc16(byte[] bytes) {
        int crc = 0xFFFF;
        for (int j = 0; j < bytes.length; j++) {
            crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
            crc ^= (bytes[j] & 0xff);
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;
        return crc;
    }

    /**
     * 获取deviceId
     */
    public static String getDeviceIdFromByte(byte[] content) throws Exception {
        if (content == null || content.length <= 8) {
            throw new Exception("获取deviceId,数据长度错误");
        }
        // 例：IMEI 号为 123456789123456，则终端 ID 为：0x01 0x23 0x45 0x67 0x89 0x12 0x34 0x56
        byte[] bytesNew = Arrays.copyOf(content, 8);
        String deviceId = byteToHex(bytesNew, false);
        if ('0' == deviceId.charAt(0)) {
            return deviceId.substring(1);
        }
        return deviceId;
    }

    public static void main(String[] args) throws Exception {
        byte[] start = new byte[]{0X78, 0X78};
        byte[] end = new byte[]{0X0D, 0X0A};

        System.out.println(byteToHex(start));
        System.out.println(byteToHex(end));

        // 例：IMEI 号为 123456789123456，则终端 ID 为：0x01 0x23 0x45 0x67 0x89 0x12 0x34 0x56
        byte[] imei = new byte[]{0x01, 0x23, 0x45, 0x67, (byte) 0x89, 0x12, 0x34, 0x56, 0X0D, 0X0A};
        String deviceIdFromByte = getDeviceIdFromByte(imei);
        System.out.println(deviceIdFromByte);

        System.out.println("--------------------------");
        System.out.println(Integer.toHexString(3).toUpperCase());
        System.out.println(Integer.toHexString(23).toUpperCase());
        System.out.println(Integer.toHexString(323).toUpperCase());

        System.out.println("--------------------------");
        String s = new String(end, "UTF-8");
        System.out.println(s.length());
        System.out.println("#" + s + "#");
    }

}
