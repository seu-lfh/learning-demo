package me.seu.demo.test.file;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SoftSysCodeWrite {
    public static void main(String[] args) throws IOException {
        List<File> allFile = getAllFile("/Users/a123/codes/codeup_ali/v2x/xc-v2x");
        ArrayList<String> strings = new ArrayList<>();
        for (File file : allFile) {
            System.out.println(file);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            //网友推荐更加简洁的写法
            while ((line = bufferedReader.readLine()) != null) {
                // 一次读入一行数据
                if (!line.equals("")) {
                    strings.add(line);
                }
            }
        }
        File writeName = new File("/Users/a123/codes/codeup_ali/v2x/xc-v2x/source.txt");
        writeName.createNewFile();
        FileWriter writer = new FileWriter(writeName);
        BufferedWriter out = new BufferedWriter(writer);
        for (String string : strings) {
            out.write(string + "\r\n");
            out.flush();
        }
    }

    public static List<File> getAllFile(String dirFilePath) {
        return getAllFile(new File(dirFilePath));
    }

    public static List<File> getAllFile(File dirFile) {
        if (Objects.isNull(dirFile) || !dirFile.exists() || dirFile.isFile())
            return null;

        File[] childrenFiles = dirFile.listFiles();
        if (Objects.isNull(childrenFiles) || childrenFiles.length == 0)
            return null;

        List<File> files = new ArrayList<>();
        for (File childFile : childrenFiles) {
            if (childFile.isFile()) {
                if (!childFile.getName().endsWith(".java")) continue;
                files.add(childFile);
            } else {
                List<File> cFiles = getAllFile(childFile);
                if (Objects.isNull(cFiles) || cFiles.isEmpty()) continue;
                files.addAll(cFiles);
            }
        }
        return files;
    }

}
