package me.seu.demo.test.aviator;

import com.alibaba.fastjson.JSONObject;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.lexer.token.OperatorType;
import com.googlecode.aviator.runtime.type.AviatorFunction;
import com.googlecode.aviator.runtime.type.AviatorObject;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 表达式引擎
 *
 * @author liangfeihu
 * @since 2021/6/27 11:48
 */
public class AviatorOne {

    public static void main(String[] args) {
        Boolean flag1 = (Boolean) AviatorEvaluator.execute(" hello != he ");
        System.out.println(flag1);

        Boolean flag2 = (Boolean) AviatorEvaluator.execute(" 346 == 246 || 4 == 7 || 5 ==6");
        System.out.println(flag2);

        String compareValue = "34";
        List<String> inputList = Arrays.asList("3", "4", "34");
        StringBuilder builder = new StringBuilder();
        for (String input : inputList) {
            builder.append(compareValue).append(" == ").append(input).append(" || ");
        }
        int index = builder.lastIndexOf("||");
        String expression = builder.substring(0, index);
        System.out.println(expression);
    }

    public static void test1() {
        Boolean flag1 = (Boolean) AviatorEvaluator.execute(" 346 == 246 ");
        System.out.println(flag1);
        Boolean flag2 = (Boolean) AviatorEvaluator.execute(" 346 <= 246 ");
        System.out.println(flag2);
        Boolean flag3 = (Boolean) AviatorEvaluator.execute(" 346 >= 246 ");
        System.out.println(flag3);

        Boolean flag4 = (Boolean) AviatorEvaluator.execute(" 10.28 == 24.6 ");
        System.out.println(flag4);
        Boolean flag5 = (Boolean) AviatorEvaluator.execute(" 3.46 <= 24.6 ");
        System.out.println(flag5);
        Boolean flag6 = (Boolean) AviatorEvaluator.execute(" 3.46 >= 24.6 ");
        System.out.println(flag6);
    }

}
