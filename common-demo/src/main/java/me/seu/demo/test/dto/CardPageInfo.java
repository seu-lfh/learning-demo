package me.seu.demo.test.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>
 * 卡分页返回信息
 * </p>
 *
 * @author liangfeihu
 * @since 2022/10/26 11:14
 */
@Setter
@Getter
public class CardPageInfo {

    /**
     * 总页数
     */
    private Integer totalPage;

    /**
     * 卡信息详情列表集合
     */
    private List<CardItemInfo> cardInfoList;

}
