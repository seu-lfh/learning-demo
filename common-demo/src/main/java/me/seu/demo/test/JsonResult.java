package me.seu.demo.test;

import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;
import me.seu.demo.utils.JacksonUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 * <p>
 * 请求结果
 * </p>
 *
 * @author liangfeihu
 * @since 2022/10/26 11:40
 */
@Setter
@Getter
public class JsonResult<T> {
    private String code;
    private String message;
    private String detail;
    private T data;

    public static void main(String[] args) {
        String cardInfo = "{\"supplierCode\":\"youfang\",\"iccidList\":[\"898604812120F1815000\",\"898604812120F1815001\",\"898604812120F1815002\",\"898604812120F1815003\",\"898604812120F1815004\",\"898604812120F1815005\",\"898604812120F1815006\",\"898604812120F1815007\",\"898604812120F1815008\",\"898604812120F1815009\",\"898604812120F1815010\",\"898604812120F1815011\",\"898604812120F1815012\",\"898604812120F1815013\",\"898604812120F1815014\",\"898604812120F1815015\",\"898604812120F1815016\",\"898604812120F1815017\",\"898604812120F1815018\",\"898604812120F1815019\",\"898604812120F1815020\",\"898604812120F1815021\",\"898604812120F1815022\",\"898604812120F1815023\",\"898604812120F1815024\",\"898604812120F1815025\",\"898604812120F1815026\",\"898604812120F1815027\",\"898604812120F1815028\",\"898604812120F1815029\",\"898604812120F1815030\",\"898604812120F1815031\",\"898604812120F1815032\",\"898604812120F1815033\",\"898604812120F1815034\",\"898604812120F1815035\",\"898604812120F1815036\",\"898604812120F1815037\",\"898604812120F1815038\",\"898604812120F1815039\",\"898604812120F1815040\",\"898604812120F1815041\",\"898604812120F1815042\",\"898604812120F1815043\",\"898604812120F1815044\",\"898604812120F1815045\",\"898604812120F1815046\",\"898604812120F1815047\",\"898604812120F1815048\",\"898604812120F1815049\",\"898604812120F1815050\",\"898604812120F1815051\",\"898604812120F1815052\",\"898604812120F1815053\",\"898604812120F1815054\",\"898604812120F1815055\",\"898604812120F1815056\",\"898604812120F1815057\",\"898604812120F1815058\",\"898604812120F1815059\",\"898604812120F1815060\",\"898604812120F1815061\",\"898604812120F1815062\",\"898604812120F1815063\",\"898604812120F1815064\",\"898604812120F1815065\",\"898604812120F1815066\",\"898604812120F1815067\",\"898604812120F1815068\",\"898604812120F1815069\",\"898604812120F1815070\",\"898604812120F1815071\",\"898604812120F1815072\",\"898604812120F1815073\",\"898604812120F1815074\",\"898604812120F1815075\",\"898604812120F1815076\",\"898604812120F1815077\",\"898604812120F1815078\",\"898604812120F1815079\",\"898604812120F1815080\",\"898604812120F1815081\",\"898604812120F1815082\",\"898604812120F1815083\",\"898604812120F1815084\",\"898604812120F1815085\",\"898604812120F1815086\",\"898604812120F1815087\",\"898604812120F1815088\",\"898604812120F1815089\",\"898604812120F1815090\",\"898604812120F1815091\",\"898604812120F1815092\",\"898604812120F1815093\",\"898604812120F1815094\",\"898604812120F1815095\",\"898604812120F1815096\",\"898604812120F1815097\",\"898604812120F1815098\",\"898604812120F1815099\"]}";
        JSONObject jsonObject = JacksonUtil.json2Obj(cardInfo, JSONObject.class);
        System.out.println(jsonObject.toJSONString());
        jsonObject = JSONObject.parseObject(jsonObject.toJSONString());
        List<String> iccidList = JSONObject.parseArray(jsonObject.getString("iccidList"), String.class);
        System.out.println(iccidList.size() + " " + iccidList);

        long count = iccidList.stream().distinct().count();
        System.out.println(count);
        if (iccidList.stream().distinct().count() != iccidList.size()) {
            System.out.println("卡重复了");
        } else {
            System.out.println("卡没有重复");
        }

        checkCardListDuplicate(iccidList);

        TreeSet treeSet = new TreeSet(iccidList);
        System.out.println("treeSet=" + treeSet);
    }

    public static void checkCardListDuplicate(List<String> iccidList)  {
        Map<String, Boolean> dupMap = new HashMap<>(iccidList.size());
        for (String iccid : iccidList) {
            if (dupMap.containsKey(iccid)) {
                System.out.println(iccid + " 重复了");
            } else {
                dupMap.put(iccid, Boolean.TRUE);
            }
        }
        dupMap.clear();
    }
}
