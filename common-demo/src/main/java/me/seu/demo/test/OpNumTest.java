package me.seu.demo.test;

public class OpNumTest {

    public static void main(String[] args) {
        int num = 1;
        System.out.println(num + " cost times = " + minTimesV3(num));
        num = 31;
        System.out.println(num + " cost times = " + minTimesV2(num));
        num = 31;
        System.out.println(num + " cost times = " + minTimesV3(num));
    }

    public static int minTimes(int num, int times) {
        if (num == 1) {
            return times;
        } else if (num == 0) {
            return times + 1;
        }

        times++;
        if (num % 2 == 1) {
            // 奇数
            return Math.min(minTimes(num + 1, times), minTimes(num - 1, times));
        } else {
            // 偶数
            return minTimes(num / 2, times);
        }
    }

    public static int minTimesV2(int num) {
        if (num == 1) {
            return 0;
        } else if (num == 0) {
            return 1;
        }

        if (num % 2 == 1) {
            // 奇数
            return Math.min(minTimesV2(num + 1), minTimesV2(num - 1)) + 1;
        } else {
            // 偶数
            return minTimesV2(num / 2) + 1;
        }
    }

    public static int minTimesV3(int num) {
        if (num == 1) {
            return 0;
        } else if (num == 0) {
            return 1;
        }
        int times = 0;
        while (num > 1) {
            times++;
            if (num % 2 == 1) {
                // 奇数 todo 何时 +1 -1
                // 奇数 todo 遇到奇数的时候，尽量让操作后的结果是4的倍数。
                if ((num + 1) % 4 == 0) {
                    num += 1;
                } else {
                    num -= 1;
                }
            } else {
                // 偶数
                num = num / 2;
            }
        }
        if (num == 0) {
            times++;
        }
        return times;
    }

}
