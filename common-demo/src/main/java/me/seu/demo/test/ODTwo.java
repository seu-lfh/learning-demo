package me.seu.demo.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ODTwo {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int c = in.nextInt();
        int b = in.nextInt();
        int[] data = new int[10];
        for (int j = 0; j < 10; j++) {
            int d = in.nextInt();
            data[j] = d;
        }
        if (b <= 0) {
            System.out.println("");
            return;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : data) {
            int bitNum = getBitNum(num) % b;
            if (bitNum < c) {
                if (map.containsKey(bitNum)) {
                    map.put(bitNum, map.get(bitNum) + 1);
                } else {
                    map.put(bitNum, 1);
                }
            }
        }
        if (map.size() <= 0) {
            System.out.println("");
            return;
        }
        int bigKey = -1;
        for (Integer key : map.keySet()) {
            Integer cnt = map.get(key);
            if (cnt > bigKey) {
                bigKey = key;
            }
        }
        System.out.println(map.get(bigKey));
    }

    private static int getBitNum(int d) {
        int one = d & 0x000000FF;
        int two = (d >> 8) & 0x000000FF;
        int three = (d >> 16) & 0x000000FF;
        int four = (d >> 24) & 0x000000FF;
        return one + two + three + four;
    }

    public static void main3(String[] args) {
        System.out.println(getBitNum(0x01010101));
        System.out.println(getBitNum(0x02010101));
        System.out.println(getBitNum(0x01030101));
    }

}
