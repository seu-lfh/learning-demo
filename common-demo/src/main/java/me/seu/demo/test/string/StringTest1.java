package me.seu.demo.test.string;

import com.alibaba.fastjson.JSON;
import me.seu.demo.test.dto.CardItemInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.aop.scope.ScopedProxyUtils;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * String Test 1
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/9 上午10:52
 */
public class StringTest1 {

    public static void main(String[] args) {
        byte b1 = 64;
        byte b2 = 35;

        System.out.println((char)b1);
        System.out.println((char)b2);
    }

    public static void main4(String[] args) {
        String testStr = "0012300";
        String replaceStr= testStr.replaceFirst("0*", "");
        System.out.println(replaceStr);

        String replaceAllStr = testStr.replaceAll("^(0+)", "");
        System.out.println(replaceAllStr);
        System.out.println("-----------------");
        System.out.println("120034" + ":" + formatFlightNo("120034"));
        System.out.println("00123" + ":" + formatFlightNo("00123"));
        System.out.println("12300" + ":" + formatFlightNo("12300"));
        System.out.println("0012300" + ":" + formatFlightNo("0012300"));

    }

    public static String formatFlightNo(String flightNo) {
        if (StringUtils.isNotBlank(flightNo)) {
            return flightNo.replaceFirst("^0*", "");
        }
        return "";
    }

    public static void main3(String[] args) {
        CardItemInfo itemInfo = new CardItemInfo();
        Set<String> fareBasisSet = new HashSet<>();
        String collectStr = fareBasisSet.stream().collect(Collectors.joining("/"));
        itemInfo.setImsi(collectStr);
        System.out.println(collectStr);
        System.out.println(JSON.toJSONString(itemInfo));
        fareBasisSet.add(null);

        System.out.println("--------------------");
        collectStr = fareBasisSet.stream().collect(Collectors.joining("/"));
        itemInfo.setMsisdn(collectStr);
        System.out.println(collectStr);
        System.out.println(JSON.toJSONString(itemInfo));
    }

    public static void main1(String[] args) {
        String a= "aaa";
        String b= "bb";
        String c= "c";
        StringBuilder sb = new StringBuilder();
        sb.append(a).append(b).append(c);
        System.out.println(MessageFormat.format(" {0} {1} {2} {3}", a, b,"",sb));
        System.out.println(MessageFormat.format(" ''{0}'' '{1}' {2} {3}", a, b,"",sb.toString()));

        System.out.println(MessageFormat.format("SEARCHING,{0}#", 16));
    }

    public static void main2(String[] args) {
        String str1 = "abc";
        String str2 = new String("abc");
        String str3 = str2.intern();
        System.out.println(str1 == str2);
        System.out.println(str2 == str3);
        System.out.println(str1 == str3);

        // 在每次赋值的时候使用 String 的 intern 方法，
        // 如果常量池中有相同值，就会重复使用该对象，返回对象引用

        String a = new String("abc").intern();
        String b = new String("abc").intern();
        if (a == b) {
            System.out.println("a==b");
        }

        String s1 = new String("1") + new String("1");
        s1.intern();
        String s2 = "11";

        // false
        System.out.println(s1 == s2);

        String s11 = new String("11") ;//+ new String("1");
        s11 = s11.intern();
        String s22 = "11";

        // true
        System.out.println(s11 == s22);
    }

}
