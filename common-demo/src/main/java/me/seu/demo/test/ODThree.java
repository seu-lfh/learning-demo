package me.seu.demo.test;

import java.util.*;

public class ODThree {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        List<Integer> codeList = new ArrayList<>();
        while (in.hasNextInt()) {
            codeList.add(in.nextInt());
        }

        if (num <= 0 || num >= 10 || codeList.size() > 100) {
            System.out.println();
        }

        int notHappyNum = 0;
        int currNum = num;
        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> waitQ = new ArrayList<>();
        map.put(codeList.get(0), 1);
        currNum--;
        for (int i = 1; i < codeList.size(); i++) {
            Integer code = codeList.get(i);
            if (map.containsKey(code)) {
                currNum++;
                map.remove(code);
            } else {
                if (currNum > 0) {
                    map.put(currNum, 1);
                    currNum--;
                } else {
                    // 是否在排队
                    if (waitQ.contains(code)) {
                        notHappyNum++;
                        waitQ.remove(code);
                    } else {
                        waitQ.add(code);
                    }
                }
            }
        }

        System.out.println(notHappyNum);
    }

}
