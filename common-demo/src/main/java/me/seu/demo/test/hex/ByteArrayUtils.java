package me.seu.demo.test.hex;

import java.util.HashMap;
import java.util.Map;

/**
 * 字节数组工具类
 *
 * @author liangfeihu
 * @since 2021/12/21 14:27
 */
public class ByteArrayUtils {

    private ByteArrayUtils() {
    }

    /**
     * 四字节数组转int
     *
     * @param b
     * @return
     */
    public static int fourByte2int(byte[] b) {
        return ((((b[0] << 24) & 0xff000000) ^ ((b[1] << 16) & 0x00ff0000))
                ^ ((b[2] << 8) & 0x0000ff00)) ^ (b[3] & 0x000000ff);
    }

    /**
     * 二字节数组转int
     *
     * @param b
     * @return
     */
    public static int twoByte2int(byte[] b) {
        return ((b[0] << 8) & 0xff00) ^ (b[1] & 0x00ff);
    }

    /**
     * int 转 二字节数组
     *
     * @param n
     * @return
     */
    public static byte[] int2twoBytes(int n) {
        byte[] buf = new byte[2];
        buf[0] = (byte) ((n >>> 8) & 0x000000ff);
        buf[1] = (byte) (n & 0x000000ff);
        return buf;
    }

    /**
     * int 转 四字节数组
     *
     * @param n
     * @return
     */
    public static byte[] int2fourBytes(int n) {
        byte[] buf = new byte[4];
        buf[0] = (byte) ((n >>> 24) & 0x000000ff);
        buf[1] = (byte) ((n >>> 16) & 0x000000ff);
        buf[2] = (byte) ((n >>> 8) & 0x000000ff);
        buf[3] = (byte) (n & 0x000000ff);
        return buf;
    }

    /**
     * 拼接两个字节数组
     *
     * @param b1
     * @param b2
     * @return
     */
    public static byte[] union(byte[] b1, byte[] b2) {
        byte[] buf = new byte[b1.length + b2.length];
        for (int i = 0; i < b1.length; i++) {
            buf[i] = b1[i];
        }
        for (int i = 0; i < b2.length; i++) {
            buf[b1.length + i] = b2[i];
        }
        return buf;
    }

    /**
     * 拼接多个字节数组
     *
     * @param b
     * @return
     */
    public static byte[] union(byte[]... b) {
        byte[] buf;
        int len = 0;
        for (int i = 0; i < b.length; i++) {
            len += b[i].length;
        }
        buf = new byte[len];
        int pos = 0;
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                buf[pos] = b[i][j];
                pos++;
            }
        }
        return buf;
    }

    /**
     * 十六进制转十进制
     *
     * @param: content
     * @return: int
     */
    public static int covert(String content) {
        int number = 0;
        String[] HighLetter = {"A", "B", "C", "D", "E", "F"};
        Map<String, Integer> map = new HashMap<>(8);
        for (int i = 0; i <= 9; i++) {
            map.put(i + "", i);
        }
        for (int j = 10; j < HighLetter.length + 10; j++) {
            map.put(HighLetter[j - 10], j);
        }
        String[] str = new String[content.length()];
        for (int i = 0; i < str.length; i++) {
            str[i] = content.substring(i, i + 1);
        }
        for (int i = 0; i < str.length; i++) {
            number += map.get(str[i]) * Math.pow(16, str.length - 1 - i);
        }
        return number;
    }

    public static void main(String[] args) {
        //String hexStr = "11a";
        String hexStr2 = "106";
        //System.out.println(covert(hexStr));
        System.out.println(covert(hexStr2));
    }


}
