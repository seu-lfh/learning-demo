package me.seu.demo.test.hex;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.netty.utils.ByteUtils;

import java.util.Arrays;

/**
 * Byte And String Test
 *
 * @author liangfeihu
 * @since 2021/7/7 17:21
 */
@Slf4j
public class ByteStringTest {

    public static void main(String[] args) throws Exception{
        byte[] startFullPacket = new byte[]{0x78, 0x78, 0x14, (byte) 0x80,
                0x0C,
                0x00, 0x01, 0x00, 0x01,
                0x52, 0x45, 0x4C, 0x41, 0x59, 0x2C, 0x30, 0x23,
                0x00, 0x02,
                0x01, 0x31, (byte) 0x8A, 0x17, 0x0D, 0x0A};

        byte[] startPacket = new byte[]{0x52, 0x45, 0x4C, 0x41, 0x59, 0x2C, 0x30, 0x23};
        System.out.println("startPacket=" + new String(startPacket, "GBK"));

        byte[] stopPacket = new byte[]{0x52, 0x45, 0x4C, 0x41, 0x59, 0x2C, 0x31, 0x23};
        System.out.println("stopPacket=" + new String(stopPacket, "GBK"));

    }

    public static void main0(String[] args) throws Exception {
        // msgBody=0x31 32 33 36 7E 7E 81 00 00 06 60 08 19 00 02 78 00 03 05 BF 00 31 32 33 36 7E
        // 0102001A60081900027805C1 313233367E7E81000006600819000278000305BF00313233367E 9D
        // 0102001A60081900027805C1 3630303831393030303237383030303030303030303030303030 9D
        byte[] byteArr = {0x31, 0x32, 0x33, 0x36, 0x7E, 0x7E, (byte) 0x81, 0x00, 0x00, 0x06, 0x60, 0x08, 0x19, 0x00, 0x02, 0x78, 0x00, 0x03, 0x05, (byte) 0xBF, 0x00, 0x31, 0x32, 0x33, 0x36, 0x7E};

        System.out.println("GBK decode=" + new String(byteArr, "GBK"));
        System.out.println("UTF-8 decode=" + new String(byteArr, "UTF-8"));
        System.out.println("Default decode=" + new String(byteArr));

        String authStr = "60081900027800000000000000";
        System.out.println(byteArr.length + " authStrLength=" + authStr.getBytes("GBK").length);
        byte[] authStrBytes = authStr.getBytes("GBK");
        System.out.println("0x" + ByteUtils.bytesToHexString(authStrBytes));

    }

    public static void main1(String[] args) throws Exception {
        byte[] ph = new byte[]{0x00, 0x00, 0x02, (byte) 0x1F};
        int phValue = ByteUtils.byteArrayToInt(ph);
        System.out.println("phValue=" + phValue);
        double phVp = phValue / 100d;
        System.out.println("phVp=" + phVp);

        byte[] instruct = new byte[]{
                //0x 0C03029C7F 0C03029C7E
                0x0C,
                0x03, 0x02, (byte) 0x9C, (byte) 0x86
                // 0x 52 45 4C 41 59 2C 31 23
                //0x52, 0x45, 0x4C, 0x41, 0x59, 0x2C, 0x31, 0x23
        };

        System.out.println("GBK decode=" + new String(instruct, "UTF-8"));
    }

    public static void main4(String[] args) {
        String deviceNo = "1234";
        System.out.println("deviceNo=" + deviceNo);
        String stringToHex = convertStringToHex(deviceNo);
        System.out.println("deviceNo=0x" + stringToHex);
        System.out.println("deviceNo=0x" + adjustHexString(stringToHex));
        System.out.println("--------------------------");
        byte[] oneBytes = new byte[]{0x31, 0x32,
                0x33, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        };

        String deviceId = byteToString(oneBytes);
        System.out.println("deviceId=" + deviceId + " length=" + deviceId.length());

    }

    public static void main3(String[] args) {
        String deviceId = "12345678901";
        String hexStr = convertStringToHex(deviceId);
        System.out.println(hexStr);
        System.out.println(Integer.toHexString(3));
        System.out.println(Integer.toHexString(39));
        System.out.println(Integer.toHexString(398));
        System.out.println(Integer.toHexString(39876));
        System.out.println(Integer.toHexString(398989843));
        System.out.println("-------------------------");

        byte[] oneBytes = new byte[]{0x31, 0x32,
                0x33, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        };
        System.out.println(byteToString(oneBytes));

        System.out.println("-------------------------");
        String hexString = ByteUtils.bytesToHexString(oneBytes);
        System.out.println("hexString: " + hexString);

        byte[] bytes = hexToByte(hexString);
        System.out.println("hexToByte: length=" + bytes.length + " 0x" + ByteUtils.bytesToHexString(bytes));

        System.out.println("-------------------------");
        String deviceNo = "1234";
        System.out.println("deviceNo=" + deviceNo);
        String stringToHex = convertStringToHex(deviceNo);
        System.out.println("deviceNo=0x" + stringToHex);
        System.out.println("deviceNo=0x" + adjustHexString(stringToHex));
    }

    public static String adjustHexString(String hexString) {
        int length = hexString.length();
        if (length > 22) {
            return hexString.substring(0, 22);
        }
        StringBuilder builder = new StringBuilder(hexString);
        int addLength = (22 - length) / 2;
        for (int i = 0; i < addLength; i++) {
            builder.append("00");
        }
        return builder.toString();
    }

    /**
     * 字节数组转为普通字符串（ASCII对应的字符）
     *
     * @param bytearray byte[]
     * @return String
     */
    public static String byteToString(byte[] byteArray) {
        StringBuilder builder = new StringBuilder();
        char temp;
        int length = byteArray.length;
        for (int i = 0; i < length; i++) {
            temp = (char) byteArray[i];
            builder.append(temp);
        }
        return builder.toString();
    }

    /**
     * 十六进制串转化为byte数组
     *
     * @return the array of byte
     */
    public static byte[] hexToByte(String hex) throws IllegalArgumentException {
        if (hex.length() % 2 != 0) {
            throw new IllegalArgumentException();
        }
        char[] arr = hex.toCharArray();
        byte[] b = new byte[hex.length() / 2];
        for (int i = 0, j = 0, l = hex.length(); i < l; i++, j++) {
            String swap = "" + arr[i++] + arr[i];
            int byteInt = Integer.parseInt(swap, 16) & 0xFF;
            b[j] = new Integer(byteInt).byteValue();
        }
        return b;
    }

    public static String convertStringToHex(String str) {
        char[] chars = str.toCharArray();
        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            hex.append(Integer.toHexString((int) chars[i]));
        }
        return hex.toString();
    }

    public static String convertHexToString(String hex) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hex.length() - 1; i += 2) {
            // grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            // convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            // convert the decimal to character
            sb.append((char) decimal);
        }
        return sb.toString();
    }

    public static void hexToDeviceIdString(String[] args) throws Exception {
        byte[] oneBytes = new byte[]{0x31, 0x32,
                0x33, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        };
        String hexString = ByteUtils.bytesToHexString(oneBytes);
        System.out.println(hexString);
        StringBuilder builder = new StringBuilder();
        for (byte b : oneBytes) {
            char c = (char) Integer.parseInt(ByteUtils.byteToHexString(b), 16);
            builder.append(c);
        }
        System.out.println(builder.toString());

        System.out.println("-------------------------");

        System.out.println(hexString);
        convertHexToString(hexString);

        System.out.println("-------------------------");
        byte[] twoBytes = new byte[]{0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31};
        String twoHexString = ByteUtils.bytesToHexString(twoBytes);
        convertHexToString(twoHexString);
    }

    public static void main2(String[] args) throws Exception {
        byte[] byteArr = {(byte) 0xC7, (byte) 0xD0, (byte) 0xB6, (byte) 0xCF, (byte) 0xD3, (byte) 0xCD, (byte) 0xB5,
                (byte) 0xE7, (byte) 0xB3, (byte) 0xC9, (byte) 0xB9, (byte) 0xA6, 0x21};
        System.out.println("GBK decode=" + new String(byteArr, "GBK"));

        String reply = "切断油电成功!";
        byte[] bytes = reply.getBytes("GBK");
        System.out.println("GBK encode=" + ByteUtilTest.byteToHex(bytes));

        //byte[] bytes1 = reply.getBytes("UTF_16BE");
        //System.out.println("UTF_16BE=" + ByteUtilTest.byteToHex(bytes1));

        // 52 45 4C 41 59 2C 31 23
        //0x5245 4C 41 59 2C 31 23
        String sendStr = "RELAY,1#";
        byte[] asciis = sendStr.getBytes("ASCII");
        System.out.println(ByteUtilTest.byteToHex(asciis));
        System.out.println("----------------------------------");


        // 78 78 19 15
        // 11 ：指令长度 17字节 = 服务器标志位 + 指令内容  (4+M)
        // 00 00 00 01 : 服务器标志位 4字节
        // C7 D0 B6 CF D3 CD B5 E7 B3 C9 B9 A6 21 ：指令内容13字节
        // 00 02 : 英文0x00 0x02
        // 10 22 AB 33 0D 0A
        // 切断油电成功!
        byte[] packetContent = new byte[]{
                0x11,
                0x00, 0x00, 0x00, 0x01,
                (byte) 0xC7, (byte) 0xD0, (byte) 0xB6, (byte) 0xCF, (byte) 0xD3, (byte) 0xCD, (byte) 0xB5,
                (byte) 0xE7, (byte) 0xB3, (byte) 0xC9, (byte) 0xB9, (byte) 0xA6, 0x21,
                0x00, 0x02
        };
        int length = packetContent.length;
        // 指令长度
        byte instruction = packetContent[0];
        byte[] serverFlag = new byte[]{packetContent[1], packetContent[2], packetContent[3], packetContent[4]};
        log.info("[TerminalReply]serverFlag=0x{}", ByteUtils.bytesToHexString(serverFlag));
        byte[] replyContent = Arrays.copyOfRange(packetContent, 5, length - 2);
        log.info("[TerminalReply]replyContent={}", new String(replyContent, "GBK"));
        byte[] language = new byte[]{packetContent[length - 2], packetContent[length - 1]};
        log.info("[TerminalReply]language=0x{}", ByteUtils.bytesToHexString(language));

    }

}
