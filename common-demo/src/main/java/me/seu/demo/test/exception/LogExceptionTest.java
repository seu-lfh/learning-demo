package me.seu.demo.test.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * 异常与日志打印测试
 *
 * @author liangfeihu
 * @since 2021/8/31 10:23
 */
@Slf4j
public class LogExceptionTest {

    public static void main(String[] args) {

        try {
            int index = 8 / 0;
        } catch (Exception e) {
            log.error("除法异常：", e);
        }

        try {
            int index2 = 9 / 0;
        } catch (Exception e2) {
            log.error("除法异常={}", e2.getMessage(), e2);
        }
    }

}
