package me.seu.demo.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ODOne {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String nextLine = in.nextLine();
        String[] s = nextLine.split(" ");
        String str1 = s[0];
        String str2 = s[1];

        if (str1.length() > str2.length()) {
            System.out.println(-1);
            return;
        }

        char[] chars = str1.toCharArray();
        List<Character> str1List = new ArrayList<>();
        for (char ch : chars) {
            str1List.add(ch);
        }
        String sortStr1 = getSortStr(str1);

        int index = -1;
        for (int i = 0; i < str2.length(); i++) {
            char c = str2.charAt(i);
            if (str1List.contains(c)) {
                int len = str2.length() - i;
                if (len < str1.length()) {
                    continue;
                }
                String substring = str2.substring(i, i + str1.length());
                String sortStr2 = getSortStr(substring);
                if (checkStr(sortStr1, sortStr2)) {
                    index = i;
                }
            }
        }
        System.out.println(index);
    }

    private static String getSortStr(String str) {
        char[] chars = str.toCharArray();
        List<Character> strList = new ArrayList<>();
        for (char ch : chars) {
            strList.add(ch);
        }
        Collections.sort(strList);
        StringBuilder stringBuilder = new StringBuilder();
        for (Character ch : strList) {
            stringBuilder.append(ch);
        }
        return stringBuilder.toString();
    }

    private static boolean checkStr(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        for (int i = 0; i < str1.length(); i++) {
            char c1 = str1.charAt(i);
            char c2 = str2.charAt(i);
            if (c1 != c2) {
                return false;
            }
        }
        return true;
    }
}