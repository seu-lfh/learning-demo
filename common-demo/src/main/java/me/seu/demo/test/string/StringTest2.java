package me.seu.demo.test.string;

import me.seu.demo.service.netty.utils.ByteUtils;

import java.util.Arrays;

/**
 * @author liangfeihu
 * @since 2021/7/9 11:36
 */
public class StringTest2 {

    private static String PROPERTY_KEY = "ViKOPKP5AoCW";
    private static String PROPERTY_POST = "/${productKey}/${deviceName}/thing/event/property/post";

    public static void main2(String[] args) {
        System.out.println(PROPERTY_POST.replace("${productKey}", PROPERTY_KEY));

        double d = 3.1415926;
        String result = String.format("%.4f", d);
        System.out.println("format=" + result);

        byte[] bytes = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
        int i = ByteUtils.byteArrayToInt(bytes);
        System.out.println(i);
    }

    public static void main3(String[] args) {
        String versionNo1 = "1.3.5";
        String versionNo2 = "1.3.6";
        String versionNo3 = "1.3.2";
        String versionNo4 = "1.3.5";

        System.out.println(versionNo1.compareTo(versionNo2));
        System.out.println(versionNo1.compareTo(versionNo3));
        System.out.println(versionNo1.compareTo(versionNo4));
    }

    public static void main1(String[] args) {
        String versionNo1 = "G40DG2022010612";
        String versionNo2 = "G40DG2022011317";

        String versionNo3 = "1.3.2";

        System.out.println(versionNo1.compareTo(versionNo2) < 0);
        System.out.println(versionNo2.compareTo(versionNo1) > 0);

        System.out.println(versionNo1.compareTo(versionNo3) > 0);
    }

    public static void main(String[] args) {
        String str = "Y/K/H";
        String[] split = str.split("/");
        System.out.println(Arrays.asList(split));
    }

}
