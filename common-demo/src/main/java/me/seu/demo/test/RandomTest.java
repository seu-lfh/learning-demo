package me.seu.demo.test;

import org.apache.commons.lang3.RandomUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * @author liangfeihu
 * @since 2021/1/11 下午6:16
 */
public class RandomTest {

    public static void main(String[] args) {
        System.out.println(RandomUtils.nextInt(0, 100));
        System.out.println(RandomUtils.nextInt(0, 100));

        System.out.println(RandomUtils.nextLong(0, 100));
        System.out.println(RandomUtils.nextLong(0, 100));

        System.out.println("--------------");

        Long birth = 522428400000L;
        Long start = 1157385600000L;
        int age = getAge(start, birth);
        System.out.println(age);

        System.out.println(UUID.randomUUID().toString());
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));

    }

    private static int getAge(Long start, Long birthday) {
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        startDate.setTime(new Date(start));
        endDate.setTime(new Date(birthday));
        return (startDate.get(Calendar.YEAR) - endDate.get(Calendar.YEAR));
    }

}
