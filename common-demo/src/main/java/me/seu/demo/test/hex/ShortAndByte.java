package me.seu.demo.test.hex;

/**
 * Short And Byte Util test
 *
 * @author liangfeihu
 * @since 2021/8/18 15:33
 */
public class ShortAndByte {

    public static void main(String[] args) {
        short num = 0x7878;
        System.out.println(num == 0x7878);
        System.out.println(num != 0x7878);

        short tail = 0x0D0A;
        System.out.println(tail);
        System.out.println(tail == 0x0D0A);
        System.out.println(tail != 0x0D0A);

        int tailNum = 3338;
        System.out.println(tailNum == 0x0D0A);
        System.out.println(tailNum != 0x0D0A);
    }

}
