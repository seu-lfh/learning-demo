package me.seu.demo.test.time;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.text.MessageFormat;

/**
 * 年月日 时分秒
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/6 下午2:22
 */
public class DateTimeTest {
    public static final String PREMISSION_STRING = "perms[{0}]";

    public static void main2(String[] args) {
        DateTime dateTime = new DateTime();
        String timeStr = dateTime.toString("yyyy-MM-dd HH:mm:ss");
        System.out.println(timeStr);
        int year = dateTime.getYear();
        int month = dateTime.getMonthOfYear();
        int day = dateTime.getDayOfMonth();

        int hour = dateTime.getHourOfDay();
        int minute = dateTime.getMinuteOfHour();
        int second = dateTime.getSecondOfMinute();

        System.out.println("年" + year + " 月" + month + " 日" + day);
        System.out.println("时" + hour + " 分" + minute + " 秒" + second);

        byte[] dt = new byte[] {0x0F, 0x0C, 0x1D, 0x00, 0x00, 0x15};
        System.out.println("年" + dt[0] + " 月" + dt[1] + " 日" + dt[2]);
        System.out.println("时" + dt[3] + " 分" + dt[4] + " 秒" + dt[5]);

        System.out.println(1183 / 100d);


        DecimalFormat formater = new DecimalFormat("000000");
        String format = formater.format(123);
        System.out.println(format);

        String hello = MessageFormat.format(PREMISSION_STRING, "hello");
        System.out.println(hello);
    }

    public static void main(String[] args) {
        LocalDateTime localDateTime1 = LocalDateTime.now().withDayOfMonth(1);
        LocalDateTime localDateTime2 = LocalDateTime.now().withDayOfMonth(1).plusMonths(-1);
        LocalDateTime localDateTime3 = LocalDateTime.now().withDayOfMonth(1).plusDays(-1);

        System.out.println(localDateTime1.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        System.out.println(localDateTime2.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        System.out.println(localDateTime3.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

}
