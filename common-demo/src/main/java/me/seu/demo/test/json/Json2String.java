package me.seu.demo.test.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Json2String test
 *
 * @author liangfeihu
 * @since 2021/6/3 16:49
 */
public class Json2String {

    @Data
    @NoArgsConstructor
    public static class JsonData {
        private String version;
        private String key;
        private Object data;
    }

    public static void main2(String[] args) {
        JSONObject object = new JSONObject();
        object.put("s1", "v1");
        object.put("s2", "v2");

        JsonData dataOne = new JsonData();
        dataOne.setVersion("v1.0");
        dataOne.setKey("key_01");
        dataOne.setData(object);
        System.out.println(JSONObject.toJSONString(dataOne));

        JSONArray objects = new JSONArray();
        objects.add(object);
        JsonData dataTwo = new JsonData();
        dataTwo.setVersion("v2.0");
        dataTwo.setKey("key_02");
        dataTwo.setData(objects);
        System.out.println(JSONObject.toJSONString(dataTwo));

    }

    @Data
    @NoArgsConstructor
    public static class ObjectData {
        private String version;
        private List<String> dataList;
    }

    public static void main(String[] args) {
        ObjectData objectData = new ObjectData();
        objectData.setVersion("v1.0.1");
        List<String> dataList = new ArrayList<>();
        dataList.add("123");
        dataList.add("456");
        dataList.add("789");
        objectData.setDataList(dataList);

        String jsonStr = JSONObject.toJSONString(objectData);
        System.out.println(jsonStr);

        JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        List<String> jsonList = JSON.parseArray(jsonObject.getString("dataList"), String.class);
        System.out.println(jsonList);
        System.out.println(JSONObject.toJSONString(jsonList));

    }

}
