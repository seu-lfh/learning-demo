package me.seu.demo.test.hex;

import me.seu.demo.service.netty.utils.ByteUtils;
import me.seu.demo.service.netty.utils.CrcItuUtils;
import me.seu.demo.utils.CRC16M;

/**
 * Crc16Test
 *
 * @author liangfeihu
 * @since 2021/7/27 10:39
 */
public class Crc16Test {

    public static void main(String[] args) {
        /*byte[] login = new byte[]{0x00, 0x03, 0x00, 0x00, 0x00, 0x01};
        String crc = getCRC(login);
        System.out.println("crc=" + crc);
        char crcLogin = CrcItuUtils.getCrc16(login, login.length);
        //crc[0]
        byte one = (byte) ((crcLogin >> 8) & 0xff);
        System.out.println(Integer.toHexString(one & 0xFF).toUpperCase());
        //crc[1]
        byte two = (byte) (crcLogin & 0xff);
        System.out.println(Integer.toHexString(two & 0xFF).toUpperCase());*/

        byte[] sbuf = ByteUtils.hexString2Buf("2222150812043902CF035A45760D0A027211151601CC0018600065490102010707");
        System.out.println(ByteUtils.bytesToHexString(sbuf));

        char crcLogin = CrcItuUtils.getCrc16(sbuf, sbuf.length);
        //crc[0]
        byte one = (byte) ((crcLogin >> 8) & 0xff);
        System.out.println(Integer.toHexString(one & 0xFF).toUpperCase());
        //crc[1]
        byte two = (byte) (crcLogin & 0xff);
        System.out.println(Integer.toHexString(two & 0xFF).toUpperCase());
    }

    /**
     * 计算CRC16校验码
     *
     * @param bytes
     * @return
     */
    public static String getCRC(byte[] bytes) {
        int CRC = 0x0000ffff;
        int POLYNOMIAL = 0x0000a001;

        int i, j;
        for (i = 0; i < bytes.length; i++) {
            CRC ^= ((int) bytes[i] & 0x000000ff);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0x00000001) != 0) {
                    CRC >>= 1;
                    CRC ^= POLYNOMIAL;
                } else {
                    CRC >>= 1;
                }
            }
        }
        return Integer.toHexString(CRC);
    }

}
