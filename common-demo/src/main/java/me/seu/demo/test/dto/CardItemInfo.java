package me.seu.demo.test.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 卡简易信息
 * </p>
 *
 * @author liangfeihu
 * @since 2022/10/26 11:14
 */
@Setter
@Getter
public class CardItemInfo {

    /**
     * iccid卡号
     */
    private String iccid;

    /**
     * 业务号
     */
    private String msisdn;

    /**
     * imsi号
     */
    private String imsi;

    /**
     * 卡片类型：1流量卡，2语音卡
     */
    private Integer cardType;

}
