package me.seu.demo.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 垫付服务同步给易安理赔系统
 *
 * @author liangfeihu
 * @since 2020/6/16 15:41
 */
@Data
@NoArgsConstructor
@ApiModel(description = "垫付工单同步请求信息")
public class PaymentAdvanceInfoSyncRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "接口编码")
    private String interfaceCode = "paymentAdvanceFromZHLToNclm";

    @ApiModelProperty(value = "请求来源：栈略")
    private String requestSource = "O-ZHL";

    @ApiModelProperty(value = "请求时间")
    private Long requestTime = System.currentTimeMillis();

    @ApiModelProperty(value = "具体请求数据")
    private RequestBodyData data;

    @ApiModel(description = "具体请求Body数据")
    @Data
    @NoArgsConstructor
    public static class RequestBodyData {

        @ApiModelProperty(value = "请求业务数据")
        private ReqBizData reqBizData;

    }

    @ApiModel(description = "业务数据")
    @Data
    @NoArgsConstructor
    public static class ReqBizData {

        @ApiModelProperty(value = "报案号")
        private String registNo;

        @ApiModelProperty(value = "保单号")
        private String policyId;

        @ApiModelProperty(value = "被保人姓名")
        private String insuredName;

        @ApiModelProperty(value = "被保人证件类型")
        private String insuredIdType;

        @ApiModelProperty(value = "被保人证件号")
        private String insuredIdNo;

        @ApiModelProperty(value = "总垫付金额")
        private Double sumPaymentAdvanceAmount;

        @ApiModelProperty(value = "垫付次数")
        private Integer sumNum;

        @ApiModelProperty(value = "出险日期")
        private Long accidentDate;

        @ApiModelProperty(value = "支付信息")
        private List<PaymentInfoDto> paymentAdvanceList;

    }


    @ApiModel(description = "支付信息")
    @Data
    @NoArgsConstructor
    public static class PaymentInfoDto {
        private Integer serialNo;
        private String paymentAdvanceTime;
        private Double paymentAdvanceAmount;
    }

}
