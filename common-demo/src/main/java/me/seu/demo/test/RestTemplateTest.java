package me.seu.demo.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import me.seu.demo.service.wechat.WxChatRobotClient;
import me.seu.demo.service.wechat.message.TextMessage;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author liangfeihu
 * @since 2020/6/17 17:49
 */
public class RestTemplateTest {

    //private static String webhook = "e931426f-75a8-4c05-822a-26598ffb7609";
    //private static String webhook = "9e83dcdf-8d9c-46b0-bb42-99b9d5b47903";
    private static String webhook = "3a0ba4b7-6a44-4ff4-b7d1-15cb63a92a00";
    private static final RestTemplate restTemplate = new RestTemplate();

    public static void main6(String[] args) {
        ResponseEntity<String> forEntity = restTemplate.getForEntity("https://iotapi.qa.tongshuyun.com/info", String.class);
        System.out.println(forEntity.getStatusCodeValue());
        System.out.println(forEntity.getBody());
    }

    public static void main(String[] args) {
        String city = "上海";
        String requestUrl = "https://api.seniverse.com/v3/weather/now.json?key=SR2alUGlIyUNH8fRH&location=" + city + "&language=zh-Hans&unit=c";
        JSONObject returnObj = restTemplate.getForObject(requestUrl, JSONObject.class);
        System.out.println(returnObj.toJSONString());

        JSONArray resultArray = returnObj.getJSONArray("results");
        JSONObject jsonObject = resultArray.getJSONObject(0);

        JSONObject location = jsonObject.getJSONObject("location");
        JSONObject now = jsonObject.getJSONObject("now");
        System.out.println(location.toJSONString());
        System.out.println(now.toJSONString());

        String dateStr = DateFormatUtils.format(new Date(), "MM月dd日 HH:mm");
        String todayWeather = dateStr + "\n" + city + "天气 " + now.getString("text") + ", 体感温度 " + now.getString("feels_like") + "˚C, 风向 " + now.getString("wind_direction") + "风，风力等级 " + now.getString("wind_scale");
        System.out.println(todayWeather);

        TextMessage message = new TextMessage(todayWeather);
        List<String> phones = new ArrayList<>();
        phones.add("15214353029");
        //phones.add("17601386603");
        //phones.add("17612166619");
        //message.setMentionedMobileList(phones);
        //message.setIsAtAll(true);
        WxChatRobotClient.send(webhook, message);

    }

    public static void main2(String[] args) {

        String requestUrl = "http://183.60.22.143/sns/ics-zlsb-service-sit.do?access_token=80169ba1e9ec4a1b83dfcdf2451ccf92&open_id=72a19b6bf67b4f3e9bacc79b1579c1e5";

        String requestBody = "{\"data\":{\"reqBizData\":{\"accidentDate\":1591261298404,\"insuredIdNo\":\"462783199103152822\",\"insuredIdType\":\"居民身份证\",\"insuredName\":\"胡送\",\"paymentAdvanceList\":[{\"paymentAdvanceAmount\":1.0,\"paymentAdvanceTime\":\"2020-06-17 17:45:04\",\"serialNo\":14}],\"policyId\":\"8G2013033202000000000294\",\"registNo\":\"62120130332020N0000003035\",\"sumNum\":1,\"sumPaymentAdvanceAmount\":1.0}},\"interfaceCode\":\"paymentAdvanceFromZHLToNclm\",\"requestSource\":\"O-ZHL\",\"requestTime\":1592450180853} ";

        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> httpEntity = new HttpEntity<String>(requestBody, headers);

        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(requestUrl, httpEntity, String.class);
        System.out.println(stringResponseEntity.getBody());
    }

}
