package me.seu.demo.list;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author liangfeihu
 * @since 2020/1/8 10:44
 */
public class ListDemo {
    public static void main2(String[] args) {
        List<String> strs = new ArrayList<>();
        strs.add("hello");
        for (String str : strs) {
            System.out.println(str);
        }
    }

    public static void main(String[] args) {
        List<String> params = new ArrayList<>();
        params.add("hello");
        params.add("java");
        params.add("world");
        System.out.println(params.stream().collect(Collectors.joining(",")));
    }
}
