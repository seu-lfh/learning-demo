package me.seu.demo.jvm.method;

/**
 * @author liangfeihu
 * @since 2020/1/11 17:00
 */
public abstract class Passenger {
    abstract void exit();

    public static void main(String[] args) {
        Passenger a = new ChinessPassenger();
        Passenger b = new ForignPassenger();

        long start = System.currentTimeMillis();

        long current = System.currentTimeMillis();
        for (int i = 1; i <= 2_000_000_000; i++) {
            if (i % 100_000_000 == 0) {
                long temp = System.currentTimeMillis();
                current = temp;
            }
            Passenger c = (i < 1_000_000_000) ? a : b;

            c.exit();
        }

        long end = System.currentTimeMillis();
        System.out.println("cost time:" + (end - start));

    }
}

class ChinessPassenger extends Passenger {
    @Override
    void exit() {

    }
}

class ForignPassenger extends Passenger {
    @Override
    void exit() {

    }
}

