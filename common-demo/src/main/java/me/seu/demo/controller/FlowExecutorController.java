package me.seu.demo.controller;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.liteflow.FirstFlowExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * LiteFlow 规则引擎
 *
 * @author liangfeihu
 * @since 2022/11/8 10:38
 */
@Slf4j
@RestController
@RequestMapping("/lite/flow")
public class FlowExecutorController {

    @Resource
    private FirstFlowExecutor firstFlowExecutor;

    @GetMapping("/config")
    public ResponseEntity testConfig() throws Exception {
        firstFlowExecutor.testConfig();
        return ResponseEntity.ok("liteFlow test config success");
    }

}
