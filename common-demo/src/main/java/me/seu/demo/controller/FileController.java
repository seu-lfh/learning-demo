package me.seu.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传 接口
 *
 * @author liangfeihu
 * @since 2022/7/14 16:46
 */
@Slf4j
@RestController
@RequestMapping("/file")
public class FileController {

    @PostMapping("/upload")
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        String fileName = file.getOriginalFilename();
        String name = file.getName();
        String contentType = file.getContentType();
        long size = file.getSize();
        int length = file.getBytes().length;
        log.info("uploadFile name={} fileName={}", name, fileName);
        log.info("uploadFile contentType={}", contentType);
        log.info("uploadFile size={} length={}", size, length);
        return ResponseEntity.ok("upload file success");
    }

}
