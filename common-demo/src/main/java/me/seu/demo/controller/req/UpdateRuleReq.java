package me.seu.demo.controller.req;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 更新场景联动规则 请求对象
 *
 * @author liangfeihu
 * @since 2021/6/22 11:21
 */
@Setter
@Getter
public class UpdateRuleReq {

    /**
     * 场景规则唯一标识Id
     */
    @NotNull(message = "场景规则唯一标识Id不能为空")
    private Long ruleId;

    /**
     * 规则名字
     */
    @NotEmpty(message = "规则名字不能为空")
    private String ruleName;

    /**
     * 规则描述
     */
    @NotEmpty(message = "规则描述不能为空")
    private String ruleDesc;

    /**
     * 0关闭 1打开
     */
    private Boolean openFlag;

    /**
     * 触发条件，1全部 2满足其一
     */
    @NotNull(message = "触发条件不能为空")
    private Integer triggerMode;

    List<TriggerItem> triggerItems;

    List<TriggerAction> triggerActions;

}
