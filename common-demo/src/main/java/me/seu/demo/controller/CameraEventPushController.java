package me.seu.demo.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.common.ObjectResult;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 萤石摄像头 事件推送
 *
 * @author liangfeihu
 * @since 2022/8/19 11:07
 */
@Slf4j
@Controller
@RequestMapping("/camera")
public class CameraEventPushController {


    @ResponseBody
    @RequestMapping(value = "/event/push", method = RequestMethod.POST, consumes = "text/plain;charset=UTF-8")
    public JSONObject auth(@RequestHeader(value = "t") Long t,
                           @RequestHeader(value = "signature", required = false) String signature,
                           @RequestHeader(value = "message_type", required = false) String message_type,
                           @RequestHeader(value = "messageType", required = false) String messageType,
                           @RequestBody String msg) {

        String pushTime = DateFormatUtils.format(new Date(t), "yyyy-MM-dd HH:mm:ss");
        log.info("[camera event] push info time={} signature={} message_type={} messageType={} ", pushTime, signature, message_type, messageType);
        JSONObject msgObject = JSONObject.parseObject(msg);
        JSONObject header = msgObject.getJSONObject("header");
        JSONObject body = msgObject.getJSONObject("body");
        log.info("[camera event] push header= {}", JSONObject.toJSONString(header));
        log.info("[camera event] push body= {}", JSONObject.toJSONString(body));

        JSONObject respObject = new JSONObject();
        respObject.put("messageId", header.getString("messageId"));
        return respObject;
    }

    @ResponseBody
    @PostMapping(value = "/event/alarm", consumes = "application/x-www-form-urlencoded;charset=UTF-8", produces = "application/json; charset=utf-8")
    public ObjectResult alarm(@RequestParam String data) {
        log.info("[camera alarm] push info={}", data);
        return ObjectResult.SUCCESS;
    }

}
