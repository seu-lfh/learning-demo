package me.seu.demo.controller.req;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 触发明细项
 *
 * @author liangfeihu
 * @since 2021/6/22 16:55
 */
@Data
@NoArgsConstructor
public class TriggerItem {

    private String productKey;

    /**
     * false不是全部设备 true是全部设备
     */
    private Boolean allFlag = Boolean.FALSE;

    private List<String> deviceList;

    /**
     * 触发条件，1属性 2上下线 3事件
     */
    private Integer triggerType;

    private JSONObject triggerDetail;

    private String cronExpression;

}
