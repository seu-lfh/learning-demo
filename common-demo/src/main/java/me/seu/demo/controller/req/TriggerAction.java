package me.seu.demo.controller.req;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 触发动作项
 *
 * @author liangfeihu
 * @since 2021/6/22 16:55
 */
@Data
@NoArgsConstructor
public class TriggerAction {

    private String productKey;

    private List<String> deviceList;

    /**
     * 动作类型：1属性 2服务
     */
    private Integer actionType;

    private String identifier;

    private String tslName;

    private Object inputParams;

    public TriggerAction(String productKey, Integer actionType, String identifier, String tslName) {
        this.productKey = productKey;
        this.actionType = actionType;
        this.identifier = identifier;
        this.tslName = tslName;
    }

}
