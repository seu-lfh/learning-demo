package me.seu.demo.serializable;

import java.io.Serializable;

/**
 * 测试序列化对象
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/3/29 下午3:34
 */
public class Student implements Serializable {

    private String name;

    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
