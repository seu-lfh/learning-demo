package me.seu.demo.serializable.parent;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 父类 A
 *
 * @author liangfeihu
 * @since 2021/7/13 10:16
 */
@Data
@NoArgsConstructor
public class A {
    private String deviceId;
    private Integer packetType;
    private Integer packetNum;
}
