package me.seu.demo.serializable.parent;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 子类 B
 *
 * @author liangfeihu
 * @since 2021/7/13 10:20
 */
@Data
@NoArgsConstructor
public class B extends A{
    private String timezone;

    public static void main(String[] args) {
        B b = new B();
        b.setDeviceId("123456");
        b.setPacketNum(1);
        b.setPacketType(10);
        b.setTimezone("东八区");
        System.out.println(JSONObject.toJSONString(b));

        String msg = "{\"deviceId\":\"123456\",\"packetNum\":1,\"packetType\":10,\"timezone\":\"东八区\"}";
        B b1 = JSONObject.parseObject(msg, B.class);
        System.out.println(b1.getDeviceId());
        System.out.println(b1.getPacketType());
        System.out.println(b1.getPacketNum());
        System.out.println(b1.getTimezone());
    }
}
