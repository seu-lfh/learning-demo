package me.seu.demo.common.msg;

import lombok.Data;

/**
 * <pre>
 * base 消息
 * </pre>
 *
 * @author liangfeihu
 * @since 2022/11/4 14:07
 */
@Data
public class BaseExchangeMessage {
    /**
     * 消息内容
     */
    private String payload;

    private Long reportTime = System.currentTimeMillis();
}
