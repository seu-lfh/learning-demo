package me.seu.demo.common;

import java.util.EnumSet;

/**
 * <pre>
 * 异常码
 * </pre>
 */
public enum ResultCode {

    /**
     * 公共异常码
     */
    SUCCESS(200, "成功"),
    INVALID_PARAMETER(10001, "参数错误"),
    SERVER_ERROR(19999, "系统错误"),

    /**
     * 业务异常
     */
    TENANT_NOT_EXIST(11000, "租户不存在"),
    ILLEGAL_OPERATION(11001, "非法操作"),

    /**
     * 航班数据异常
     */
    BASE_FLIGHT_NOT_EXIST(21000, "航班数据不存在"),
    BASE_FLIGHT_HAS_EXIST(21001, "航班数据已存在"),
    BASE_FLIGHT_HAS_ABOLISH(21002, "航班数据已作废"),
    BASE_FLIGHT_OPERATING_FLIGHT_NO_NOT_EXIST(21003, "承运航班号不能为空"),

    PRODUCT_TASK_NOT_EXIST(22000, "商品任务不存在"),
    PRODUCT_TASK_HAS_EXIST(22001, "商品任务已存在"),
    PRODUCT_TASK_CAN_NOT_UPDATE(22002, "商品任务已生成商品,不可编辑"),
    PRODUCT_CAN_NOT_UPDATE(22003, "商品已生成商品,不可编辑"),

    TASK_TRAVEL_DATE_INVALID(22005, "旅行日期格式不正确"),
    TASK_TRAVEL_DATE_STEP_TOO_LONG(22006, "最早日期和最晚日期间隔不超过361天"),
    TASK_TRAVEL_DATE_START_END_INVALID(22007, "回程日期必须晚于等于去程日期"),
    TASK_TRAVEL_DATE_GE_TODAY(22008, "旅行日期必须等于或晚于今天"),
    ;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    private static final EnumSet<ResultCode> RESULT_CODES = EnumSet.allOf(ResultCode.class);

    public static final ResultCode getResultCode(String code) {
        for (ResultCode resultCode : RESULT_CODES) {
            if (resultCode.getCode().equals(code)) {
                return resultCode;
            }
        }
        return null;
    }
}
