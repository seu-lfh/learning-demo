package me.seu.demo.common;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 通用返回值
 *
 * @author liangfeihu
 * @since 2022/8/23 16:42
 */
@Data
@NoArgsConstructor
public class ObjectResult<T> implements Serializable {

    private static final long serialVersionUID = -9146805371831100892L;

    final public static ObjectResult SUCCESS = new ObjectResult("200", "操作成功");

    final public static ObjectResult ERROR = new ObjectResult("400", "操作失败");

    final public static ObjectResult EXCEPTION = new ObjectResult("500", "服务异常");

    public ObjectResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 返回码
     */
    private String code;

    /**
     * 返回事件
     */
    private String msg;

    /**
     * 返回数据
     */
    private T data;

}
