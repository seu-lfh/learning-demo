package me.seu.demo.common.msg;

import lombok.Data;

/**
 * <pre>
 * tcp消息体
 * </pre>
 *
 *
 * @author liangfeihu
 * @since 2022/11/4 14:08
 */
@Data
public class TcpExchangeMessage extends BaseExchangeMessage {
    private String identifier;
    private String productKey;
    private String deviceNo;
}
