package me.seu.demo.common.msg;

import lombok.Getter;
import lombok.Setter;

/**
 * <pre>
 * coap消息
 * </pre>
 *
 * @Author xinguang.huang
 * @Date 2021/5/18
 **/
@Setter
@Getter
public class CoapExchangeMessage extends BaseExchangeMessage {
    /**
     * 消息topic
     */
    private String topic;
}
