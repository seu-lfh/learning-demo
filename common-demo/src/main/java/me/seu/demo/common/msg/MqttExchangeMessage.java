package me.seu.demo.common.msg;

import lombok.Getter;
import lombok.Setter;

/**
 * <pre>
 * mqtt消息
 * </pre>
 *
 * @author liangfeihu
 * @since 2022/11/4 14:08
 */
@Setter
@Getter
public class MqttExchangeMessage extends BaseExchangeMessage {
    /**
     * 消息topic
     */
    private String topic;
}
