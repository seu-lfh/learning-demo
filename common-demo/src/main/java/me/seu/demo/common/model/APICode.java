package me.seu.demo.common.model;

/**
 * 响应状态枚举类接口
 *
 * @author liangfeihu
 */
public interface APICode {

    /**
     * 状态码
     */
    int getCode();

    /**
     * 状态信息
     */
    String getMessage();
}