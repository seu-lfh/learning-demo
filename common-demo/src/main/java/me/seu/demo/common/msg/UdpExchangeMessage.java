package me.seu.demo.common.msg;

import lombok.Data;

/**
 * <pre>
 * udp消息
 * </pre>
 *
 * @Author xinguang.huang
 * @Date 2021/7/12
 **/
@Data
public class UdpExchangeMessage extends BaseExchangeMessage {
    private String identifier;
    private String productKey;
    private String deviceNo;
}
