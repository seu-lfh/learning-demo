package me.seu.demo.common.msg;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <pre>
 * http restful 消息 (http协议设备)
 * </pre>
 *
 * @author liangfeihu
 * @since 2021/8/25 11:46
 */
@Data
@NoArgsConstructor
public class HttpExchangeMessage extends BaseExchangeMessage {
    /**
     * 消息topic
     */
    private String topic;
}
