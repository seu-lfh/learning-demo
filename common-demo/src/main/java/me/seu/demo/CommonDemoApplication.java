package me.seu.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动类
 *
 * @author liangfeihu
 * @since 2019/12/30 18:20
 */
@Slf4j
@EnableScheduling
@SpringBootApplication
public class CommonDemoApplication implements ApplicationContextAware {

    private static ApplicationContext applicationContext = null;

    /**
     * Set the ApplicationContext that this object runs in.
     * Normally this call will be used to initialize the object.
     *
     * @param applicationContext the ApplicationContext object to be used by this object
     * @throws BeansException
     * @see BeanInitializationException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        CommonDemoApplication.applicationContext = applicationContext;
    }

    public static void main(String[] args) {
        SpringApplication.run(CommonDemoApplication.class, args);

        //org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext
        log.info("ApplicationContext={}", applicationContext);
        log.info("ApplicationContext name={}", applicationContext.getApplicationName());
        log.info("ApplicationContext parent={} parent BF={}", applicationContext.getParent(), applicationContext.getParentBeanFactory());
        log.info("HealthyController bean = {}", applicationContext.getBean("healthyController"));
    }

}
