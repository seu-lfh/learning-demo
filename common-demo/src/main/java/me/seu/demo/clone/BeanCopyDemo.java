package me.seu.demo.clone;

import me.seu.demo.clone.dto.Person;
import me.seu.demo.clone.dto.Son;
import org.springframework.beans.BeanUtils;

/**
 * @author liangfeihu
 * @since 2020/1/16 14:37
 */
public class BeanCopyDemo {
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("person");
        person.setAge(28);
        person.setHobby("旅游");

        System.out.println(person);

        //子类
        Son son = new Son();
        //copy
        BeanUtils.copyProperties(person, son);

        System.out.println(son);

        System.out.println("---------------------");
        Son son1 = new Son();
        son1.setHobby("看电影");
        son1.setName("绝无敌");
        son1.setAge(99);
        System.out.println(son1);

        Person p1 = new Person();
        BeanUtils.copyProperties(son1, p1);
        System.out.println(p1);
    }
}
