package me.seu.demo.clone.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @author liangfeihu
 * @since 2020/1/16 14:40
 */
@ToString
@Data
public class Person {
    /**
     * 属性名相同，类型相同
     */
    private String name;
    /**
     * 属性名相同，类型不同
     */
    private Integer age;

    private String hobby;
}
