package me.seu.demo.algorithms;

import java.util.HashSet;
import java.util.Set;

/**
 * 唯一摩尔斯密码词
 *
 * @author liangfeihu
 * @since 2021/2/20 下午5:33
 */
public class MorseRepresentations {

    public static int uniqueMorseRepresentationsV(String[] words) {

        String[] codes = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
        Set<String> set = new HashSet<>();
        for (String word: words) {
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < word.length(); i++) {
                int index = word.charAt(i) - 'a';
                res.append(codes[index]);
            }
            set.add(res.toString());
        }
        System.out.println(set.toString());

        return set.size();
    }

    public static void main(String[] args) {
        String[] words = {"gin", "zen", "gig", "msg"};
        System.out.println(uniqueMorseRepresentationsV(words));
    }

}
