package me.seu.demo.exception;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.common.CommonResult;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器
 *
 * @author liangfeihu
 * @since 2020/3/7 10:55
 */
@Slf4j
@RestControllerAdvice
public class RestExceptionHandler {

    /**
     * 处理自定义异常
     */
    @ExceptionHandler(RestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public CommonResult handleRestException(RestException e){
        log.error("RestException ", e);
        return CommonResult.failed(e.getCode(), e.getMsg());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public CommonResult handleException(Exception e){
        log.error("系统异常了 ", e);
        return CommonResult.failed();
    }


}
