package me.seu.demo.lambda;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试类
 * @author liangfeihu
 * @since 2020/3/4 16:38
 */
public class EatActionTest {
    public static void main(String[] args) {
        Person person = new Person("Lily");
        EatApple eatApple = new EatApple();
        EatBanana eatBanana = new EatBanana();
        List<EatFruit> eatList = new ArrayList<>();
        eatList.add(eatApple);
        eatList.add(eatBanana);
        eatList.add(person.getPersonEatAction());

        EatActions eatActions = new EatActions(eatList);
        eatActions.eatAction();
    }
}
