package me.seu.demo.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 浮点数工具类
 *
 * @author liangfeihu
 * @since 2022/6/28 15:31
 */
public class BigDecimalUtil {

    public static BigDecimal formatRefundFee(BigDecimal refundFee) {
        return refundFee.setScale(0, RoundingMode.HALF_UP);
    }

    public static void main(String[] args) {
        BigDecimal decimal1 = new BigDecimal("5.89");
        BigDecimal decimal2 = new BigDecimal("5.03");

        BigDecimal decimal3 = decimal1.setScale(0, RoundingMode.HALF_UP);
        System.out.println(decimal3);

        BigDecimal decimal4 = decimal2.setScale(0, RoundingMode.HALF_UP);
        System.out.println(decimal4);
    }
}
