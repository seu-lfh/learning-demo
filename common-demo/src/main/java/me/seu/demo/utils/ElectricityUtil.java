package me.seu.demo.utils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 电压电量对应关系工具类
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/3/22 上午11:45
 */
public class ElectricityUtil {
    private static final Integer LOW_VOLTAGE = 3650;
    private static final Integer HIGH_VOLTAGE = 4200;

    private static final Map<Integer, Double> electricMap;

    static {
        Map<Integer, Double> tempMap = new HashMap(64);
        tempMap.put(3650, 0.00d);
        tempMap.put(3660, 0.02d);
        tempMap.put(3670, 0.05d);
        tempMap.put(3680, 0.07d);
        tempMap.put(3690, 0.10d);
        tempMap.put(3700, 0.12d);
        tempMap.put(3710, 0.15d);
        tempMap.put(3720, 0.17d);
        tempMap.put(3730, 0.20d);
        tempMap.put(3740, 0.25d);
        tempMap.put(3750, 0.30d);
        tempMap.put(3760, 0.35d);
        tempMap.put(3770, 0.37d);
        tempMap.put(3780, 0.40d);
        tempMap.put(3790, 0.45d);

        tempMap.put(3810, 0.50d);
        tempMap.put(3820, 0.52d);
        tempMap.put(3830, 0.54d);
        tempMap.put(3840, 0.55d);
        tempMap.put(3850, 0.57d);
        tempMap.put(3860, 0.59d);
        tempMap.put(3870, 0.60d);
        tempMap.put(3880, 0.62d);
        tempMap.put(3890, 0.64d);

        tempMap.put(3900, 0.68d);
        tempMap.put(3910, 0.70d);
        tempMap.put(3920, 0.71d);
        tempMap.put(3930, 0.72d);
        tempMap.put(3940, 0.73d);
        tempMap.put(3950, 0.74d);
        tempMap.put(3960, 0.75d);
        tempMap.put(3970, 0.77d);
        tempMap.put(3980, 0.78d);
        tempMap.put(3990, 0.79d);

        tempMap.put(4000, 0.80d);
        tempMap.put(4010, 0.81d);
        tempMap.put(4020, 0.83d);
        tempMap.put(4030, 0.84d);
        tempMap.put(4040, 0.85d);
        tempMap.put(4050, 0.87d);
        tempMap.put(4060, 0.88d);
        tempMap.put(4070, 0.89d);
        tempMap.put(4080, 0.90d);
        tempMap.put(4090, 0.91d);

        tempMap.put(4100, 0.92d);
        tempMap.put(4110, 0.93d);
        tempMap.put(4120, 0.94d);
        tempMap.put(4130, 0.95d);
        tempMap.put(4140, 0.96d);
        tempMap.put(4150, 0.97d);
        tempMap.put(4160, 0.98d);
        tempMap.put(4170, 0.99d);
        tempMap.put(4180, 0.99d);
        tempMap.put(4190, 0.99d);
        tempMap.put(4200, 1.00d);

        electricMap = Collections.unmodifiableMap(tempMap);
    }

    /**
     * 根据电压预测电量
     *
     * @param voltage 电压（单位 毫伏）
     * @return 电量
     */
    public static BigDecimal getElectricityByVoltage(Integer voltage) {
        if (voltage == null || voltage < LOW_VOLTAGE || voltage > HIGH_VOLTAGE) {
            return null;
        }
        int num = voltage % 10;
        voltage = (num >= 5) ? (voltage - num + 10) : (voltage - num);
        Double eVal = electricMap.get(voltage);
        return BigDecimal.valueOf(eVal);
    }

    public static void main(String[] args) {
        System.out.println(getElectricityByVoltage(3888));
        System.out.println(getElectricityByVoltage(3913));
        System.out.println(getElectricityByVoltage(4086));
        System.out.println(getElectricityByVoltage(4134));
    }

}
