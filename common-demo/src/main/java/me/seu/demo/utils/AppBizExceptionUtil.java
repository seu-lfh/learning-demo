package me.seu.demo.utils;

import me.seu.demo.common.AppBizException;
import me.seu.demo.common.ResultCode;

public class AppBizExceptionUtil {

    public static AppBizException build(ResultCode code) {
        return new AppBizException(String.valueOf(code.getCode()), code.getMessage());
    }

}
