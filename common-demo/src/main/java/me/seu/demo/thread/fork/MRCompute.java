package me.seu.demo.thread.fork;

import java.util.Map;
import java.util.concurrent.ForkJoinPool;

/**
 * MapReduce模拟统计文本单词数量
 *
 * @author liangfeihu
 * @since 2020/3/18 15:46
 */
public class MRCompute {
    public static void main(String[] args) {
        String[] fc = {"hello world",
                "hello me",
                "hello fork",
                "hello join",
                "fork join in world"};
        //创建ForkJoin线程池
        ForkJoinPool fjp = new ForkJoinPool(3);
        //创建任务
        MR mr = new MR(fc, 0, fc.length);
        //启动任务
        Map<String, Long> result = fjp.invoke(mr);
        //输出结果
        result.forEach((k, v) -> System.out.println(k + ":" + v));
    }
}
