package me.seu.demo.thread.fork;

import java.util.concurrent.ForkJoinPool;

/**
 * 计算斐波那契数列
 *
 * @author liangfeihu
 * @since 2020/3/18 15:25
 */
public class FibonacciCompute {

    public static void main(String[] args) {
        //1、创建分治任务线程池
        ForkJoinPool fjp = new ForkJoinPool(4);
        //2、创建分治任务
        Fibonacci fib = new Fibonacci(30);
        //3、启动分治任务
        Integer result = fjp.invoke(fib);
        //4、输出结果
        System.out.println(result);
    }

}
