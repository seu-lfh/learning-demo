package me.seu.demo.common;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Kafka 消息
 *
 * @author liangfeihu
 * @since 2020/3/9 12:04
 */
@Data
@NoArgsConstructor
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String msg;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;
}
