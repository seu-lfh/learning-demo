package me.seu.demo.service;


import me.seu.demo.model.Picture;

import java.util.List;

/**
 * Picture文档操作接口
 *
 * @author liangfeihu
 * @since 2020/4/27 19:10
 */
public interface PictureService {

    /**
     * 获取一页记录
     *
     * @param current
     * @param rowCount
     * @param sortId
     * @return
     */
    List<Picture> getPicList(int current, int rowCount, String sortId);

    /**
     * 获取总数目
     *
     * @return
     */
    int getPictureNum();

    /**
     * 使用主键获取记录
     *
     * @param id
     * @return
     */
    Picture getPictureById(String id);

    /**
     * 保存与更新文档
     *
     * @param p
     */
    void saveOrUpdatePicture(Picture p);

    /**
     * 删除文档
     *
     * @param id
     */
    void deletePicture(String id);

    /**
     * 获取一页搜索结果
     *
     * @param current
     * @param rowCount
     * @param sortId
     * @param search
     * @return
     */
    List<Picture> getSearchResult(int current, int rowCount, String sortId, String search);

    /**
     * 获取搜索结果总数
     *
     * @param search
     * @return
     */
    int getSearchResultTotal(String search);

}
