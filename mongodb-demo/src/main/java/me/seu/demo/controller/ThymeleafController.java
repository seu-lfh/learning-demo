package me.seu.demo.controller;

import me.seu.demo.model.Picture;
import me.seu.demo.service.PictureService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * MongoDB页面
 *
 * @author liangfeihu
 * @since 2020/4/28 11:08
 */
@Controller
@RequestMapping(value = "/mongodb")
public class ThymeleafController {

    @Resource(name = "pictureService")
    private PictureService pictureService;


    @RequestMapping(value = "/index")
    public String showPic() {
        return "mongodb";
    }

    /**
     * 这个方法被新增和修改请求共用,如果是新增，id必须为空，否则是修改请求
     */
    @RequestMapping(value = "/picture", method = RequestMethod.POST)
    public String addPicture(@ModelAttribute("picture") Picture picture) {
        pictureService.saveOrUpdatePicture(picture);
        return "mongodb";
    }

}
