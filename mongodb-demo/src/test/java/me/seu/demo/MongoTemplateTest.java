package me.seu.demo;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.model.Picture;
import org.bson.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

/**
 * Test MongoTemplate API
 *
 * @author liangfeihu
 * @since 2020/4/28 14:08
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MongoDemoApplication.class)
public class MongoTemplateTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void testGetCollection() {
        String collectionName = mongoTemplate.getCollectionName(Picture.class);
        log.info("collectionName = {}", collectionName);
        Set<String> collectionNames = mongoTemplate.getCollectionNames();
        log.info("collectionNames = {}", collectionNames);

        MongoCollection<Document> collection = mongoTemplate.getCollection(collectionName);
        long countDocuments = collection.countDocuments();
        log.info("countDocuments = {}", countDocuments);

        DistinctIterable<String> filenameList = mongoTemplate.getCollection(collectionName).distinct("filename", String.class);
        for (String str : filenameList) {
            log.info("filenameList distinct ={}", str);
        }
    }



}
