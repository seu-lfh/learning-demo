package me.seu.demo.delay;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * 延时消息 消费者
 *
 * @author liangfeihu
 * @since 2020/4/17 11:30
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "delay-topic", consumerGroup = "my-consumer_delay-topic")
public class DelayConsumer implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt msgExt) {
        try {
            log.info("[{}]received delay msg sendTime=[{}] receiveTime=[{}] body=[{}]", Thread.currentThread().getName(), msgExt.getProperty("SEND_TIME"),
                    DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"), new String(msgExt.getBody(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
