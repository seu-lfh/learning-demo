package me.seu.demo.consumer;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.domain.OrderPaidEvent;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * object 消费者
 *
 * @author liangfeihu
 * @since 2020/4/16 15:41
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "test-topic-2", consumerGroup = "my-consumer_test-topic-2", messageModel = MessageModel.BROADCASTING)
public class ObjectConsumer implements RocketMQListener<OrderPaidEvent> {

    @Override
    public void onMessage(OrderPaidEvent orderPaidEvent) {
        log.info("received orderPaidEvent: {}", orderPaidEvent);
    }

}
