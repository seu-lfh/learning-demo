package me.seu.demo.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * String msg 消费者2
 *
 * @author liangfeihu
 * @since 2020/4/16 15:40
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "test-topic-1", consumerGroup = "my-consumer2_test-topic-1", messageModel = MessageModel.CLUSTERING)
public class MyConsumer2 implements RocketMQListener<String> {

    @Override
    public void onMessage(String message) {
        log.info("[MyConsumer2]received message: {}", message);
    }

}
