package me.seu.demo.transaction;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

/**
 * 事务消息 消费者
 *
 * @author liangfeihu
 * @since 2020/4/17 11:30
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "transaction-topic", consumerGroup = "my-consumer_transaction-topic")
public class TransactionConsumer implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt msgExt) {
        try {
            log.info("[{}] received transaction msg tag={} body=[{}]", Thread.currentThread().getName(), msgExt.getTags(), new String(msgExt.getBody(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
